package com.zomart.app.app;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiEnterpriseConfig;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.gson.Gson;
import com.zomart.app.models.store_model.StoreObject;


/**
 * MyAppPrefsManager handles some Prefs of AndroidShopApp Application
 **/


public class MyAppPrefsManager {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor prefsEditor;
    
    private int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "AndroidWooShop_Prefs";
    
    private static final String USER_LANGUAGE_ID  = "language_ID";
    private static final String USER_ID = "userID";
    private static final String USER_LANGUAGE_CODE  = "language_Code";
    private static final String CURRENCY_CODE = "currency_code";
    private static final String IS_USER_LOGGED_IN = "isLogged_in";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String SELECTED_STORE = "selected_store";
    private static final String MY_LOCATION_LAT = "my_lat";
    private static final String MY_LOCATION_LNG = "my_lng";
    private static final String WELCOME_SHOWN = "welcome_shown";
    private static final String IS_PUSH_NOTIFICATIONS_ENABLED = "isPushNotificationsEnabled";
    private static final String IS_LOCAL_NOTIFICATIONS_ENABLED = "isLocalNotificationsEnabled";

    private static final String LOCAL_NOTIFICATIONS_TITLE = "localNotificationsTitle";
    private static final String LOCAL_NOTIFICATIONS_DURATION = "localNotificationsDuration";
    private static final String LOCAL_NOTIFICATIONS_DESCRIPTION = "localNotificationsDescription";



    public MyAppPrefsManager(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        prefsEditor = sharedPreferences.edit();
    }
    
    
    public void setUserLanguageId(int langID) {
        prefsEditor.putInt(USER_LANGUAGE_ID, langID);
        prefsEditor.commit();
    }
    
    public Integer getUserLanguageId() {
        return sharedPreferences.getInt(USER_LANGUAGE_ID, 1);
    }

    public void setSelectedStore(StoreObject store){
        prefsEditor.putString(SELECTED_STORE,new Gson().toJson(store));
        prefsEditor.commit();
    }

    public StoreObject getSelectedStore(){
        try {
            StoreObject store = new Gson().fromJson(sharedPreferences.getString(SELECTED_STORE, "{}"), StoreObject.class);
            return store;
        }catch (Exception e){
            return new StoreObject();
        }
    }

    public void setWelcomeShown(boolean is){
        prefsEditor.putBoolean(WELCOME_SHOWN,is);
        prefsEditor.commit();
    }

    public boolean getWelcomeShown(){
        return sharedPreferences.getBoolean(WELCOME_SHOWN,false);
    }

    public void setMyLat(String lat){
        prefsEditor.putString(MY_LOCATION_LAT,lat);
        prefsEditor.commit();
    }

    public void setMyLng(String lng){
        prefsEditor.putString(MY_LOCATION_LNG,lng);
        prefsEditor.commit();
    }

    public String getMyLat(){
        return sharedPreferences.getString(MY_LOCATION_LAT,"");
    }

    public String getMyLng(){
        return sharedPreferences.getString(MY_LOCATION_LNG,"");
    }

    public void setUserID(int userID){
        prefsEditor.putInt(USER_ID,userID);
        prefsEditor.commit();
    }
    
    public Integer getUserID(){
        return sharedPreferences.getInt(USER_ID,0);
    }
    
    public void setUserLanguageCode(String langCode) {
        prefsEditor.putString(USER_LANGUAGE_CODE, langCode);
        prefsEditor.commit();
    }
    
    public String getUserLanguageCode() {
        return sharedPreferences.getString(USER_LANGUAGE_CODE, "en");
    }
    
    public  void setCurrencyCode(String currencyCode){
        prefsEditor.putString(CURRENCY_CODE,currencyCode);
        prefsEditor.commit();
    }
    
    public String getCurrencyCode(){
        return sharedPreferences.getString(CURRENCY_CODE, "USD");
    }
    
    public void setUserLoggedIn(boolean isUserLoggedIn) {
        prefsEditor.putBoolean(IS_USER_LOGGED_IN, isUserLoggedIn);
        prefsEditor.commit();
    }

    public boolean isUserLoggedIn() {
        return sharedPreferences.getBoolean(IS_USER_LOGGED_IN, false);
    }


    public void setFirstTimeLaunch(boolean isFirstTimeLaunch) {
        prefsEditor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTimeLaunch);
        prefsEditor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return sharedPreferences.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setPushNotificationsEnabled(boolean isPushNotificationsEnabled) {
        prefsEditor.putBoolean(IS_PUSH_NOTIFICATIONS_ENABLED, isPushNotificationsEnabled);
        prefsEditor.commit();
    }

    public boolean isPushNotificationsEnabled() {
        return sharedPreferences.getBoolean(IS_PUSH_NOTIFICATIONS_ENABLED, true);
    }

    public void setLocalNotificationsEnabled(boolean isLocalNotificationsEnabled) {
        prefsEditor.putBoolean(IS_LOCAL_NOTIFICATIONS_ENABLED, isLocalNotificationsEnabled);
        prefsEditor.commit();
    }

    public boolean isLocalNotificationsEnabled() {
        return sharedPreferences.getBoolean(IS_LOCAL_NOTIFICATIONS_ENABLED, true);
    }


    public void setLocalNotificationsTitle(String localNotificationsTitle) {
        prefsEditor.putString(LOCAL_NOTIFICATIONS_TITLE, localNotificationsTitle);
        prefsEditor.commit();
    }

    public String getLocalNotificationsTitle() {
        return sharedPreferences.getString(LOCAL_NOTIFICATIONS_TITLE, "Zomart");
    }

    public void setLocalNotificationsDuration(String localNotificationsDuration) {
        prefsEditor.putString(LOCAL_NOTIFICATIONS_DURATION, localNotificationsDuration);
        prefsEditor.commit();
    }

    public String getLocalNotificationsDuration() {
        return sharedPreferences.getString(LOCAL_NOTIFICATIONS_DURATION, "day");
    }

    public void setLocalNotificationsDescription(String localNotificationsDescription) {
        prefsEditor.putString(LOCAL_NOTIFICATIONS_DESCRIPTION, localNotificationsDescription);
        prefsEditor.commit();
    }

    public String getLocalNotificationsDescription() {
        return sharedPreferences.getString(LOCAL_NOTIFICATIONS_DESCRIPTION, "Check bundle of new Products");
    }








    private static final String APP_PREFERENCE_NAME = "connect_pref";



    public  class AddressProperties {
        //private static SessionManager.AddressProperties ourInstance = new SessionManager.AddressProperties();

//        public static SessionManager.AddressProperties getInstance() {
//            return ourInstance;
//        }

        public void setPickUpAddressKey(String value,Context context ) {
            SharedPreferences pref = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("PickUpAddressKey", value).apply();
        }

        public String getPickUpAddressKey(Context context )  {
            SharedPreferences prefs = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("PickUpAddressKey", "");
        }

        public void setPickUpAddress(String value,Context context ) {
            SharedPreferences pref = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("PickUpAddress", value).apply();
        }

        public String getPickUpAddress(Context context ) {
            SharedPreferences prefs = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("PickUpAddress", "");
        }

        public void setDeliveryAddressKey(String value,Context context ) {
            SharedPreferences pref = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("DeliveryAddressKey", value).apply();
        }

        public String getDeliveryAddressKey(Context context ) {
            SharedPreferences prefs = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("DeliveryAddressKey", "");
        }

        public void setDeliveryAddress(String value,Context context ) {
            SharedPreferences pref = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("DeliveryAddress", value).apply();
        }

        public String getDeliveryAddress(Context context ) {
            SharedPreferences prefs = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("DeliveryAddress", "");
        }

        public void setPickUpDate(String value,Context context ) {
            SharedPreferences pref = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("PickUpDate", value).apply();
        }

        public String getPickUpDate(Context context ) {
            SharedPreferences prefs = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("PickUpDate", "");
        }

        public void setPickUpTime(String value,Context context ) {
            SharedPreferences pref = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("PickUpTime", value).apply();
        }

        public String getPickUpTime(Context context ) {
            SharedPreferences prefs = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("PickUpTime", "");
        }

        public void setDeliveryDate(String value,Context context ) {
            SharedPreferences pref = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("DeliveryDate", value).apply();
        }

        public String getDeliveryDate(Context context ) {
            SharedPreferences prefs = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("DeliveryDate", "");
        }

        public void setDeliveryTime(String value,Context context ) {
            SharedPreferences pref = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("DeliveryTime", value).apply();
        }

        public String getDeliveryTime(Context context ) {
            SharedPreferences prefs = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("DeliveryTime", "");
        }

    }

    public static class LocationProperties {
        private LocationProperties ourInstance = new LocationProperties();

        public static void setLatitude(String valueOf) {
        }

        public  LocationProperties getInstance() {
            return ourInstance;
        }

        public void setCityKey(String value,Context context ) {
            SharedPreferences pref = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("cityKey", value).apply();
        }

        public String getCityKey(Context context ) {
            SharedPreferences prefs = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("cityKey", "");
        }

        public void setCityName(String value,Context context ) {
            SharedPreferences pref = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("cityName", value).apply();
        }

        public String getCityName(Context context ) {
            SharedPreferences prefs = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("cityName", "");
        }

        public void setAreaKey(String value,Context context ) {
            SharedPreferences pref = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("areaKey", value).apply();
        }

        public String getAreaKey(Context context ) {
            SharedPreferences prefs = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("areaKey", "");
        }

        public void setAreaName(String value,Context context ) {
            SharedPreferences pref = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("areaName", value).apply();
        }

        public String getAreaName(Context context ) {
            SharedPreferences prefs = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("areaName", "");
        }

        public static void setLatitude(String value,Context context ) {
            SharedPreferences pref = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("latitude", value).apply();
        }

        public String getLatitude(Context context ) {
            SharedPreferences prefs = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("latitude", "");
        }

        public static void setLongitude(String value,Context context ) {
            SharedPreferences pref = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("Longitude", value).apply();
        }

        public String getLongitude(Context context ) {
            SharedPreferences prefs = context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("Longitude", "");
        }

    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        View focusedView = activity.getCurrentFocus();
        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }



}
