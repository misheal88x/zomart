package com.zomart.app.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.material.snackbar.Snackbar;

import com.zomart.app.app.App;
import com.zomart.app.app.MyAppPrefsManager;
import com.zomart.app.models.device_model.AppSettingsDetails;

import com.zomart.app.R;

import com.zomart.app.utils.Utilities;
import com.zomart.app.constant.ConstantValues;
import com.zomart.app.network.StartAppRequests;

import org.json.JSONObject;

import androidx.core.app.ActivityCompat;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;


/**
 * SplashScreen activity, appears on App Startup
 **/

public class SplashScreen extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{
    View rootView;
    ProgressBar progressBar;

    MyTask myTask;
    static StartAppRequests startAppRequests;
    MyAppPrefsManager myAppPrefsManager;
    String strProduct,strSellerInfo;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 10;

    private String selected_type = "";
    private String saved_store_id = "",saved_product_id = "";
    //---------------------------------------------------------------------------
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 199;
    //---------------------------------------------------------------------------

    @Override
    public void onStart() {
        super.onStart();
        Branch branch = Branch.getInstance(this);

        // Branch init
        branch.initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    // params are the deep linked params associated with the link that the user clicked -> was re-directed to this app
                    // params will be empty if no data found
                    // ... insert custom logic here ...
                    strSellerInfo = referringParams.optString(ConstantValues.STORE_ID);
                    strProduct = referringParams.optString(ConstantValues.PRODUCT_ID);

                    Log.i("BRANCH SDK", referringParams.toString());


                } else {
                    Log.i("BRANCH SDK", error.getMessage());
                }
            }
        }, this.getIntent().getData(), this);
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.setIntent(intent);
    }

    public void FullScreencall() {
        if(Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) { // lower api
            View v = this.getWindow().getDecorView();
            v.setSystemUiVisibility(View.GONE);
        } else if(Build.VERSION.SDK_INT >= 19) {
            //for new api versions.
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash);
        FullScreencall();

        Log.i("zomart", "zomart_Version= "+ConstantValues.CODE_VERSION);

        // Bind Layout Views
        progressBar = (ProgressBar) findViewById(R.id.splash_loadingBar);
        rootView = progressBar;

        MainJob();

        /*
        if (isOnline()){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED&&
                            ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED&&
                            ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED&&
                            ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                        requestRuntimePermission();
                    }else {
                        if (checkPlayServices()){
                            //---------------------------------------------------------------------------
                            mGoogleApiClient = new GoogleApiClient.Builder(SplashScreen.this)
                                    .addApi(LocationServices.API)
                                    .addConnectionCallbacks(SplashScreen.this)
                                    .addOnConnectionFailedListener(SplashScreen.this).build();
                            mGoogleApiClient.connect();

                            //-----------------------------------------------------------------------------
                        }
                    }
                }
            },0);
        }else {
            AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreen.this);
            builder.setMessage(getResources().getString(R.string.no_internet_dialog)).
                    setCancelable(false).
                    setPositiveButton(getResources().getString(R.string.no_internet_ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }).show();
        }


         */
    }


    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode!= ConnectionResult.SUCCESS){
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)){
                GooglePlayServicesUtil.getErrorDialog(resultCode,this,PLAY_SERVICES_RESOLUTION_REQUEST).show();

            }else {
                Toast.makeText(this, getResources().getString(R.string.no_google_play_services), Toast.LENGTH_SHORT).show();
                finish();
            }
            return false;
        }
        return true;
    }


    private void requestRuntimePermission() {
        ActivityCompat.requestPermissions(this,new String[]
                {
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                },11);
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case 11:{
                if (grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED
                        && grantResults[1]== PackageManager.PERMISSION_GRANTED
                        && grantResults[2]== PackageManager.PERMISSION_GRANTED
                        && grantResults[3]== PackageManager.PERMISSION_GRANTED){
                    if (checkPlayServices()){
                        //---------------------------------------------------------------------------
                        mGoogleApiClient = new GoogleApiClient.Builder(SplashScreen.this)
                                .addApi(LocationServices.API)
                                .addConnectionCallbacks(SplashScreen.this)
                                .addOnConnectionFailedListener(SplashScreen.this).build();
                        mGoogleApiClient.connect();

                        //-----------------------------------------------------------------------------
                    }
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreen.this);
                    builder.setMessage(getResources().getString(R.string.permission_denied)).setCancelable(false).
                            setPositiveButton(getResources().getString(R.string.no_internet_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent(Intent.ACTION_MAIN);
                                    intent.addCategory(Intent.CATEGORY_HOME);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();
                                }
                            }).show();
                }
                break;
            }
        }
    }

    //*********** Sets App configuration ********//

    private void setAppConfig() {

        AppSettingsDetails appSettings = ((App) getApplicationContext()).getAppSettingsDetails();

        if (appSettings != null) {

            if (appSettings.getAppName() != null  &&  !TextUtils.isEmpty(appSettings.getAppName())) {
                ConstantValues.APP_HEADER = appSettings.getAppName();
            } else {
                ConstantValues.APP_HEADER = getString(R.string.app_name);

            }

          /*  if (appSettings.getCurrencySymbol() != null  &&  !TextUtils.isEmpty(appSettings.getCurrencySymbol())) {
                ConstantValues.CURRENCY_SYMBOL = appSettings.getCurrencySymbol();
            } else {
                ConstantValues.CURRENCY_SYMBOL = "$";
            }*/

          /*  if (appSettings.getCurrencyName() != null  &&  !TextUtils.isEmpty(appSettings.getCurrencyName())) {
                ConstantValues.CURRENCY_CODE = appSettings.getCurrencyName();
            } else {
                ConstantValues.CURRENCY_CODE = "USD";
            }
            */

            if (appSettings.getFilterMaxPrice() != null  &&  !TextUtils.isEmpty(appSettings.getFilterMaxPrice())) {
                ConstantValues.FILTER_MAX_PRICE = appSettings.getFilterMaxPrice();
            } else {
                ConstantValues.FILTER_MAX_PRICE = "1000";
            }

            if(appSettings.getWp_multi_currency() !=null && "1".equalsIgnoreCase(appSettings.getWp_multi_currency())){
                ConstantValues.CURRENCY_CODE = myAppPrefsManager.getCurrencyCode();
                ConstantValues.CURRENCY_SYMBOL = Utilities.getCurrencySymbol(ConstantValues.CURRENCY_CODE);
            }
            else {
                ConstantValues.CURRENCY_CODE = appSettings.getCurrencyName();
                ConstantValues.CURRENCY_SYMBOL = Utilities.getCurrencySymbol(ConstantValues.CURRENCY_CODE);
            }

            if (appSettings.getNewProductDuration() != null  &&  !TextUtils.isEmpty(appSettings.getNewProductDuration())) {
                ConstantValues.NEW_PRODUCT_DURATION = Long.parseLong(appSettings.getNewProductDuration());
            } else {
                ConstantValues.NEW_PRODUCT_DURATION = 30;
            }

            ConstantValues.DEFAULT_HOME_STYLE = getString(R.string.actionHome) +" "+ appSettings.getHomeStyle();
            ConstantValues.DEFAULT_CATEGORY_STYLE = getString(R.string.actionCategory) +" "+ appSettings.getCategoryStyle();

            ConstantValues.IS_GUEST_CHECKOUT_ENABLED = ("yes".equalsIgnoreCase(appSettings.getGuestCheckout()));
            ConstantValues.IS_ONE_PAGE_CHECKOUT_ENABLED = ("1".equalsIgnoreCase(appSettings.getOnePageCheckout()));

            ConstantValues.IS_GOOGLE_LOGIN_ENABLED = ("1".equalsIgnoreCase(appSettings.getGoogleLogin()));
            ConstantValues.IS_FACEBOOK_LOGIN_ENABLED = ("1".equalsIgnoreCase(appSettings.getFacebookLogin()));
            ConstantValues.IS_ADD_TO_CART_BUTTON_ENABLED = ("1".equalsIgnoreCase(appSettings.getCartButton()));

            ConstantValues.IS_ADMOBE_ENABLED = ("1".equalsIgnoreCase(appSettings.getAdmob()));
            ConstantValues.ADMOBE_ID = appSettings.getAdmobId();
            ConstantValues.AD_UNIT_ID_BANNER = appSettings.getAdUnitIdBanner();
            ConstantValues.AD_UNIT_ID_INTERSTITIAL = appSettings.getAdUnitIdInterstitial();

            ConstantValues.ABOUT_US = appSettings.getAboutPage();
            ConstantValues.TERMS_SERVICES = appSettings.getTermsPage();
            ConstantValues.PRIVACY_POLICY = appSettings.getPrivacyPage();
            ConstantValues.REFUND_POLICY = appSettings.getRefundPage();

            myAppPrefsManager.setLocalNotificationsTitle(appSettings.getNotificationTitle());
            myAppPrefsManager.setLocalNotificationsDuration(appSettings.getNotificationDuration());
            myAppPrefsManager.setLocalNotificationsDescription(appSettings.getNotificationText());

        } else {
            ConstantValues.APP_HEADER = getString(R.string.app_name);

            ConstantValues.CURRENCY_SYMBOL = "$";
            ConstantValues.FILTER_MAX_PRICE = "10000";
            ConstantValues.NEW_PRODUCT_DURATION = 30;

            ConstantValues.IS_GUEST_CHECKOUT_ENABLED =  false;
            ConstantValues.IS_ONE_PAGE_CHECKOUT_ENABLED = false;

            ConstantValues.IS_ADMOBE_ENABLED = false;
            ConstantValues.IS_GOOGLE_LOGIN_ENABLED = false;
            ConstantValues.IS_FACEBOOK_LOGIN_ENABLED = false;
            ConstantValues.IS_ADD_TO_CART_BUTTON_ENABLED = true;

            ConstantValues.DEFAULT_HOME_STYLE = getString(R.string.actionHome) +" "+ 1;
            ConstantValues.DEFAULT_CATEGORY_STYLE = getString(R.string.actionCategory) +" "+ 1;

            ConstantValues.ABOUT_US = getString(R.string.lorem_ipsum);
            ConstantValues.TERMS_SERVICES = getString(R.string.lorem_ipsum);
            ConstantValues.PRIVACY_POLICY = getString(R.string.lorem_ipsum);
            ConstantValues.REFUND_POLICY = getString(R.string.lorem_ipsum);
        }

    }

    private void MainJob(){
        // Initializing StartAppRequests and PreferencesManager
        startAppRequests = new StartAppRequests(this);
        myAppPrefsManager = new MyAppPrefsManager(this);

        ConstantValues.LANGUAGE_CODE = myAppPrefsManager.getUserLanguageCode();

        ConstantValues.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();
        ConstantValues.IS_PUSH_NOTIFICATIONS_ENABLED = myAppPrefsManager.isPushNotificationsEnabled();
        ConstantValues.IS_LOCAL_NOTIFICATIONS_ENABLED = myAppPrefsManager.isLocalNotificationsEnabled();

        // Start MyTask after 3 seconds
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                myTask = new MyTask();
                myTask.execute();
            }
        }, 3000);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:

                        MainJob2();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    SplashScreen.this,
                                    REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                    default:{
                        MainJob2();
                    }
                }
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode,resultCode,data);
        Log.d("onActivityResult()", Integer.toString(resultCode));

        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode)
        {
            case REQUEST_LOCATION:
                switch (resultCode)
                {
                    case Activity.RESULT_OK:
                    {
                        // All required changes were successfully made
                        Toast.makeText(SplashScreen.this, getResources().getString(R.string.gps_on), Toast.LENGTH_LONG).show();
                        MainJob2();
                        break;
                    }
                    case Activity.RESULT_CANCELED:
                    {
                        MainJob2();
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
                break;
        }
    }



    /************* MyTask is Inner Class, that handles StartAppRequests on Background Thread *************/

    private class MyTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            // Check for Internet Connection from the static method of Helper class
            if (Utilities.isNetworkAvailable(SplashScreen.this)) {

                // Call the method of StartAppRequests class to process App Startup Requests
                startAppRequests.StartRequests();

                return "1";
            } else {
                return "0";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result.equalsIgnoreCase("0")) {

                progressBar.setVisibility(View.GONE);

                // No Internet Connection
                Snackbar.make(rootView, getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {

                            // Handle the Retry Button Click
                            @Override
                            public void onClick(View v) {

                                progressBar.setVisibility(View.VISIBLE);

                                // Restart MyTask after 3 seconds
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        myTask = new MyTask();
                                        myTask.execute();
                                    }
                                }, 3000);
                            }
                        })
                        .show();

            }
            else {
                setAppConfig();

                if (myAppPrefsManager.isFirstTimeLaunch()) {
                    // Navigate to IntroScreen
                    if (isGPSOn()){
                        startActivity(new Intent(getBaseContext(), IntroScreen.class));
                        finish();
                    }else {
                        if (myAppPrefsManager.getWelcomeShown()){
                            selected_type = "intro";
                            startLocationServices();
                        }else {
                            Intent intent = new Intent(SplashScreen.this,WelcomeOneActivity.class);
                            intent.putExtra("type","intro");
                            startActivity(intent);
                            finish();
                        }
                    }
                } else {

                    if(!strProduct.isEmpty()||!strSellerInfo.isEmpty()){
                        //Intent i = new Intent(getBaseContext(), MainActivity.class);
                        if (isGPSOn()){
                            if (myAppPrefsManager.getSelectedStore().getId() !=0 && !myAppPrefsManager.getMyLat().equals("")
                            &&!myAppPrefsManager.getMyLng().equals("")){
                                Intent i = new Intent(getBaseContext(), MainActivity.class);
                                if(!strProduct.isEmpty()) {
                                    i.putExtra(ConstantValues.PRODUCT_ID, strProduct);
                                }
                                else {
                                    i.putExtra(ConstantValues.STORE_ID, strSellerInfo);
                                }
                                startActivity(i);
                            }else {
                                Intent i = new Intent(getBaseContext(), PickLocationActivity.class);
                                if(!strProduct.isEmpty()) {
                                    i.putExtra(ConstantValues.PRODUCT_ID, strProduct);
                                }
                                else {
                                    i.putExtra(ConstantValues.STORE_ID, strSellerInfo);
                                }
                                startActivity(i);
                            }
                            finish();
                        }else {

                            if (myAppPrefsManager.getWelcomeShown()){
                                if(!strProduct.isEmpty()) {
                                    saved_product_id = strProduct;
                                } else {
                                    saved_store_id = strSellerInfo;
                                }
                                selected_type = "pick";
                                startLocationServices();
                            }else {
                                Intent intent = new Intent(SplashScreen.this,WelcomeOneActivity.class);
                                intent.putExtra("type","pick");
                                if(!strProduct.isEmpty()) {
                                    intent.putExtra(ConstantValues.PRODUCT_ID,strProduct);
                                } else {
                                    intent.putExtra(ConstantValues.STORE_ID,strSellerInfo);
                                }
                                startActivity(intent);
                                finish();
                            }

                        }
                    }
                    else {
                        // Navigate to MainActivity
                        //startActivity(new Intent(getBaseContext(), MainActivity.class));
                        if (isGPSOn()){
                            if (myAppPrefsManager.getSelectedStore().getId()!=0&&
                            !myAppPrefsManager.getMyLat().equals("")&&
                            !myAppPrefsManager.getMyLng().equals("")){
                                startActivity(new Intent(getBaseContext(), MainActivity.class));
                            }else {
                                startActivity(new Intent(getBaseContext(), PickLocationActivity.class));
                            }
                            finish();
                        }else {
                            if (myAppPrefsManager.getWelcomeShown()){
                                selected_type = "pick";
                                startLocationServices();
                            }else {
                                Intent intent = new Intent(SplashScreen.this,WelcomeOneActivity.class);
                                intent.putExtra("type","pick");
                                startActivity(intent);
                                finish();
                            }
                        }
                    }
                    new MyExtraTasks().execute();
                }
            }
        }

    }

    public static class MyExtraTasks extends AsyncTask<String,Void,String>{

        @Override
        protected String doInBackground(String... strings) {
            // Call the method of StartAppRequests class to process App Startup Requests
            startAppRequests.StartRequestExtra();

            return "true";
        }
    }

    public boolean isOnline(){
        try{
            ConnectivityManager conMgr =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
            if (conMgr != null){
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null){
                    return false;
                }else {
                    return true;
                }
            }else {
                return false;
            }
        }catch (Exception e){
            Toast.makeText(this, "Crashed because of isOnline function", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private boolean isGPSOn(){
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            return false;
        }else {
            return true;
        }
    }

    private void startLocationServices(){
        if (isOnline()){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED&&
                            ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED&&
                            ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED&&
                            ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                        requestRuntimePermission();
                    }else {
                        if (checkPlayServices()){
                            //---------------------------------------------------------------------------
                            mGoogleApiClient = new GoogleApiClient.Builder(SplashScreen.this)
                                    .addApi(LocationServices.API)
                                    .addConnectionCallbacks(SplashScreen.this)
                                    .addOnConnectionFailedListener(SplashScreen.this).build();
                            mGoogleApiClient.connect();

                            //-----------------------------------------------------------------------------
                        }
                    }
                }
            },0);
        }else {
            AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreen.this);
            builder.setMessage(getResources().getString(R.string.no_internet_dialog)).
                    setCancelable(false).
                    setPositiveButton(getResources().getString(R.string.no_internet_ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }).show();
        }
    }

    private void MainJob2(){
        Intent intent = null;
        if (selected_type.equals("pick")) {
            if (myAppPrefsManager.getSelectedStore().getId()!=0&&
                    !myAppPrefsManager.getMyLat().equals("")&&
                    !myAppPrefsManager.getMyLng().equals("")){
                intent = new Intent(SplashScreen.this, MainActivity.class);
            }else {
                intent = new Intent(SplashScreen.this, PickLocationActivity.class);
            }
        }else if (selected_type.equals("intro")){
            intent = new Intent(SplashScreen.this, IntroScreen.class);
        }
            if (saved_store_id!=null &&!saved_store_id.equals("")){
                intent.putExtra(ConstantValues.STORE_ID,saved_store_id);
            }
            if (saved_product_id!=null &&!saved_product_id.equals("")){
                intent.putExtra(ConstantValues.PRODUCT_ID,saved_product_id);
            }
            startActivity(intent);
            finish();

    }
}

