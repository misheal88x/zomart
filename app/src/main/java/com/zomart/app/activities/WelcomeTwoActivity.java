package com.zomart.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.zomart.app.R;
import com.zomart.app.app.MyAppPrefsManager;
import com.zomart.app.constant.ConstantValues;

public class WelcomeTwoActivity extends AppCompatActivity {

    private MyAppPrefsManager myAppPrefsManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_two);
        myAppPrefsManager = new MyAppPrefsManager(WelcomeTwoActivity.this);
        findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                if (getIntent().getStringExtra("type")!=null){
                    if (getIntent().getStringExtra("type").equals("intro")){
                        intent = new Intent(WelcomeTwoActivity.this,IntroScreen.class);
                    }else if (getIntent().getStringExtra("type").equals("pick")){
                        if (myAppPrefsManager.getSelectedStore().getId()!=0&&
                        !myAppPrefsManager.getMyLat().equals("")&&
                        !myAppPrefsManager.getMyLng().equals("")){
                            intent = new Intent(WelcomeTwoActivity.this,MainActivity.class);
                        }else {
                            intent = new Intent(WelcomeTwoActivity.this,PickLocationActivity.class);
                        }
                    }
                    if (getIntent().getStringExtra(ConstantValues.PRODUCT_ID)!=null){
                        intent.putExtra(ConstantValues.PRODUCT_ID,getIntent().getStringExtra(ConstantValues.PRODUCT_ID));
                    }
                    if (getIntent().getStringExtra(ConstantValues.STORE_ID)!=null){
                        intent.putExtra(ConstantValues.STORE_ID,getIntent().getStringExtra(ConstantValues.STORE_ID));
                    }
                    startActivity(intent);
                    finish();
                }
            }
        });
    }
}
