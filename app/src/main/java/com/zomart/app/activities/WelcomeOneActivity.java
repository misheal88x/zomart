package com.zomart.app.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.zomart.app.R;
import com.zomart.app.app.MyAppPrefsManager;
import com.zomart.app.constant.ConstantValues;
import com.zomart.app.utils.LocaleHelper;

public class WelcomeOneActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 10;
    //---------------------------------------------------------------------------
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 199;
    //---------------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_one);
        findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainJob();
            }
        });

        if (isOnline()){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (ActivityCompat.checkSelfPermission(WelcomeOneActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED&&
                            ActivityCompat.checkSelfPermission(WelcomeOneActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED&&
                            ActivityCompat.checkSelfPermission(WelcomeOneActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED&&
                            ActivityCompat.checkSelfPermission(WelcomeOneActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                        requestRuntimePermission();
                    }else {
                        if (checkPlayServices()){
                            //---------------------------------------------------------------------------
                            mGoogleApiClient = new GoogleApiClient.Builder(WelcomeOneActivity.this)
                                    .addApi(LocationServices.API)
                                    .addConnectionCallbacks(WelcomeOneActivity.this)
                                    .addOnConnectionFailedListener(WelcomeOneActivity.this).build();
                            mGoogleApiClient.connect();

                            //-----------------------------------------------------------------------------
                        }
                    }
                }
            },0);
        }else {
            AlertDialog.Builder builder = new AlertDialog.Builder(WelcomeOneActivity.this);
            builder.setMessage(getResources().getString(R.string.no_internet_dialog)).
                    setCancelable(false).
                    setPositiveButton(getResources().getString(R.string.no_internet_ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }).show();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {

        String languageCode = ConstantValues.LANGUAGE_CODE;
        if ("".equalsIgnoreCase(languageCode))
            languageCode = ConstantValues.LANGUAGE_CODE = "en";

        super.attachBaseContext(LocaleHelper.wrapLocale(newBase, languageCode));
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode!= ConnectionResult.SUCCESS){
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)){
                GooglePlayServicesUtil.getErrorDialog(resultCode,this,PLAY_SERVICES_RESOLUTION_REQUEST).show();

            }else {
                Toast.makeText(this, getResources().getString(R.string.no_google_play_services), Toast.LENGTH_SHORT).show();
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:

                        findViewById(R.id.next).setVisibility(View.VISIBLE);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    WelcomeOneActivity.this,
                                    REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                    default:{
                        findViewById(R.id.next).setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void requestRuntimePermission() {
        ActivityCompat.requestPermissions(this,new String[]
                {
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                },11);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case 11:{
                if (grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED
                        && grantResults[1]== PackageManager.PERMISSION_GRANTED
                        && grantResults[2]== PackageManager.PERMISSION_GRANTED
                        && grantResults[3]== PackageManager.PERMISSION_GRANTED){
                    if (checkPlayServices()){
                        //---------------------------------------------------------------------------
                        mGoogleApiClient = new GoogleApiClient.Builder(WelcomeOneActivity.this)
                                .addApi(LocationServices.API)
                                .addConnectionCallbacks(WelcomeOneActivity.this)
                                .addOnConnectionFailedListener(WelcomeOneActivity.this).build();
                        mGoogleApiClient.connect();

                        //-----------------------------------------------------------------------------
                    }
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(WelcomeOneActivity.this);
                    builder.setMessage(getResources().getString(R.string.permission_denied)).setCancelable(false).
                            setPositiveButton(getResources().getString(R.string.no_internet_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent(Intent.ACTION_MAIN);
                                    intent.addCategory(Intent.CATEGORY_HOME);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();
                                }
                            }).show();
                }
                break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        Log.d("onActivityResult()", Integer.toString(resultCode));

        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode)
        {
            case REQUEST_LOCATION:
                switch (resultCode)
                {
                    case Activity.RESULT_OK:
                    {
                        // All required changes were successfully made
                        Toast.makeText(WelcomeOneActivity.this, getResources().getString(R.string.gps_on), Toast.LENGTH_LONG).show();
                        findViewById(R.id.next).setVisibility(View.VISIBLE);
                        break;
                    }
                    case Activity.RESULT_CANCELED:
                    {
                        findViewById(R.id.next).setVisibility(View.VISIBLE);
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
                break;
        }
    }

    public boolean isOnline(){
        try{
            ConnectivityManager conMgr =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
            if (conMgr != null){
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null){
                    return false;
                }else {
                    return true;
                }
            }else {
                return false;
            }
        }catch (Exception e){
            Toast.makeText(this, "Crashed because of isOnline function", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private void MainJob(){
        MyAppPrefsManager myAppPrefsManager = new MyAppPrefsManager(WelcomeOneActivity.this);
        myAppPrefsManager.setWelcomeShown(true);
        Intent intent = new Intent(WelcomeOneActivity.this,WelcomeTwoActivity.class);
        intent.putExtra("type",getIntent().getStringExtra("type"));
        if (getIntent().getStringExtra(ConstantValues.PRODUCT_ID)!=null){
            intent.putExtra(ConstantValues.PRODUCT_ID,getIntent().getStringExtra(ConstantValues.PRODUCT_ID));
        }
        if (getIntent().getStringExtra(ConstantValues.STORE_ID)!=null){
            intent.putExtra(ConstantValues.STORE_ID,getIntent().getStringExtra(ConstantValues.STORE_ID));
        }
        startActivity(intent);
        finish();
    }
}
