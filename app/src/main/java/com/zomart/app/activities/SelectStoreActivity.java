package com.zomart.app.activities;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Transformers.BaseTransformer;
import com.google.gson.Gson;
import com.zomart.app.R;
import com.zomart.app.adapters.StoresAdapter;
import com.zomart.app.app.App;
import com.zomart.app.app.MyAppPrefsManager;
import com.zomart.app.constant.ConstantValues;
import com.zomart.app.fragments.My_Orders;
import com.zomart.app.fragments.Points_Fragment;
import com.zomart.app.fragments.Product_Description;
import com.zomart.app.fragments.Products;
import com.zomart.app.fragments.SelectStoreFragment;
import com.zomart.app.fragments.Update_Account;
import com.zomart.app.models.api_response_model.ErrorResponse;
import com.zomart.app.models.banner_model.BannerDetails;
import com.zomart.app.models.store_model.StoreObject;
import com.zomart.app.network.APIClient;
import com.zomart.app.utils.LocaleHelper;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.zomart.app.app.App.getContext;

public class SelectStoreActivity extends AppCompatActivity{



    //todo must send PRODUCT_ID & STORE_ID to SelectStoreFragment

    private LinearLayout lyt_market,lyt_orders,lyt_rewards,lyt_account;
    private ImageView img_market,img_orders,img_rewards,img_account;
    private TextView tv_market,tv_orders,tv_rewards,tv_account;

    private FragmentManager fragmentManager;
    private Fragment fragment;
    Toolbar toolbar;
    TextView title;
    ActionBar actionBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_store);
        init_views();
        init_events();
        init_activity();
    }

    private void init_views() {
        //Linear Layouts
        lyt_market = findViewById(R.id.tab_market_layout);
        lyt_orders = findViewById(R.id.tab_orders_layout);
        lyt_rewards = findViewById(R.id.tab_rewards_layout);
        lyt_account = findViewById(R.id.tab_account_layout);
        //ImageView
        img_market = findViewById(R.id.tab_market_icon);
        img_orders = findViewById(R.id.tab_orders_icon);
        img_rewards = findViewById(R.id.tab_rewards_icon);
        img_account = findViewById(R.id.tab_account_icon);
        //TextViews
        tv_market = findViewById(R.id.tab_market_txt);
        tv_orders = findViewById(R.id.tab_orders_txt);
        tv_rewards = findViewById(R.id.tab_rewards_txt);
        tv_account = findViewById(R.id.tab_account_txt);
        //Fragment Manager
        fragmentManager = getSupportFragmentManager();
        //Toolbar
        toolbar = (Toolbar) findViewById(R.id.myToolbar);
        title = toolbar.findViewById(R.id.toolbar_title);
    }

    private void init_events() {
        lyt_market.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_market();
            }
        });

        lyt_orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConstantValues.IS_USER_LOGGED_IN){
                    click_orders();
                }else {
                    Intent intent = new Intent(SelectStoreActivity.this,Login.class);
                    intent.putExtra("is_from_select_store",true);
                    startActivity(intent);
                    finish();
                }
            }
        });

        lyt_rewards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConstantValues.IS_USER_LOGGED_IN){
                    click_rewards();
                }else {
                    Intent intent = new Intent(SelectStoreActivity.this,Login.class);
                    intent.putExtra("is_from_select_store",true);
                    startActivity(intent);
                    finish();
                }
            }
        });

        lyt_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConstantValues.IS_USER_LOGGED_IN){
                    click_account();
                }else {
                    Intent intent = new Intent(SelectStoreActivity.this,Login.class);
                    intent.putExtra("is_from_select_store",true);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private void init_activity() {
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        title.setText(getResources().getString(R.string.select_store_title));
        click_market();
    }


    @Override
    protected void attachBaseContext(Context newBase) {

        String languageCode = ConstantValues.LANGUAGE_CODE;
        if ("".equalsIgnoreCase(languageCode))
            languageCode = ConstantValues.LANGUAGE_CODE = "en";

        super.attachBaseContext(LocaleHelper.wrapLocale(newBase, languageCode));
    }

    @Override
    public void onBackPressed() {

        Fragment f = fragmentManager.findFragmentById(R.id.main_fragment);
        if (f instanceof SelectStoreFragment){
            finish();
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);
        }else if (f instanceof My_Orders||
        f instanceof Points_Fragment||
        f instanceof Update_Account){
            click_market();
        }else {
            fragmentManager.popBackStack();
        }
    }

    public void click_market(){
        img_market.setImageResource(R.drawable.ic_market_on);
        img_orders.setImageResource(R.drawable.ic_calendar_off);
        img_rewards.setImageResource(R.drawable.ic_points_off);
        img_account.setImageResource(R.drawable.ic_profile_off);

        tv_market.setTextColor(getResources().getColor(R.color.colorPrimary));
        tv_orders.setTextColor(getResources().getColor(R.color.light_grey));
        tv_rewards.setTextColor(getResources().getColor(R.color.light_grey));
        tv_account.setTextColor(getResources().getColor(R.color.light_grey));

        Bundle bundle = new Bundle();
        List<BannerDetails> l = ((App) getApplicationContext()).getBannersList();
        bundle.putString("banners",new Gson().toJson(((App) getApplicationContext()).getBannersList()));
        if (getIntent().getStringExtra(ConstantValues.PRODUCT_ID)!=null){
            if (!getIntent().getStringExtra(ConstantValues.PRODUCT_ID).equals("")){
                bundle.putString(ConstantValues.PRODUCT_ID,getIntent().getStringExtra(ConstantValues.PRODUCT_ID));
            }
        }
        if (getIntent().getStringExtra(ConstantValues.STORE_ID)!=null){
            if (!getIntent().getStringExtra(ConstantValues.STORE_ID).equals("")){
                bundle.putString(ConstantValues.STORE_ID,getIntent().getStringExtra(ConstantValues.STORE_ID));
            }
        }

        // Navigate to Select Store Fragment
        fragment = new SelectStoreFragment();
        fragment.setArguments(bundle);
        fragmentManager.beginTransaction()
                .replace(R.id.main_fragment, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack(null)
                .commit();
    }

    public void click_orders(){
        img_market.setImageResource(R.drawable.ic_market_off);
        img_orders.setImageResource(R.drawable.ic_calendar_on);
        img_rewards.setImageResource(R.drawable.ic_points_off);
        img_account.setImageResource(R.drawable.ic_profile_off);

        tv_market.setTextColor(getResources().getColor(R.color.light_grey));
        tv_orders.setTextColor(getResources().getColor(R.color.colorPrimary));
        tv_rewards.setTextColor(getResources().getColor(R.color.light_grey));
        tv_account.setTextColor(getResources().getColor(R.color.light_grey));


        // Navigate to My Orders Fragment
        fragment = new My_Orders();
        fragmentManager.beginTransaction()
                .replace(R.id.main_fragment, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack(null)
                .commit();
    }

    public void click_rewards(){
        img_market.setImageResource(R.drawable.ic_market_off);
        img_orders.setImageResource(R.drawable.ic_calendar_off);
        img_rewards.setImageResource(R.drawable.ic_points_on);
        img_account.setImageResource(R.drawable.ic_profile_off);

        tv_market.setTextColor(getResources().getColor(R.color.light_grey));
        tv_orders.setTextColor(getResources().getColor(R.color.light_grey));
        tv_rewards.setTextColor(getResources().getColor(R.color.colorPrimary));
        tv_account.setTextColor(getResources().getColor(R.color.light_grey));

        Bundle bundle = new Bundle();
        bundle.putBoolean("isMenuItem", true);

        // Navigate to Points Fragment
        fragment = new Points_Fragment();
        fragment.setArguments(bundle);
        fragmentManager.beginTransaction()
                .replace(R.id.main_fragment, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack(null)
                .commit();
    }

    public void click_account(){
        img_market.setImageResource(R.drawable.ic_market_off);
        img_orders.setImageResource(R.drawable.ic_calendar_off);
        img_rewards.setImageResource(R.drawable.ic_points_off);
        img_account.setImageResource(R.drawable.ic_profile_on);

        tv_market.setTextColor(getResources().getColor(R.color.light_grey));
        tv_orders.setTextColor(getResources().getColor(R.color.light_grey));
        tv_rewards.setTextColor(getResources().getColor(R.color.light_grey));
        tv_account.setTextColor(getResources().getColor(R.color.colorPrimary));

        // Navigate to Account Fragment
        fragment = new Update_Account();
        fragmentManager.beginTransaction()
                .replace(R.id.main_fragment, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack(null)
                .commit();
    }

}
