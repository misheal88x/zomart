package com.zomart.app.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.zomart.app.R;
import com.zomart.app.app.MyAppPrefsManager;
import com.zomart.app.constant.ConstantValues;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

//import com.zomart.app.databinding.ActivityPickLocationBinding;
public class PickLocationActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener ,
        AdapterView.OnItemClickListener, View.OnClickListener{

    private GoogleMap mMap;
    private String currentLatitude = "", currentLongitude = "";
    private FloatingActionButton location_fab,road_fab;
    private Marker marker;
    private String str;




    //---------------------------------------------------------------------------
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 199;
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    Double mCurrentLattitude = 0.0, mCurrentLongitude = 0.0;
    private double latitude;
    private double longitude;
    private Button btn_skip;
    private FloatingActionButton btn_location;
    private AutoCompleteTextView tvSelectedAddress;
    private Activity mActivity;
    private MyAppPrefsManager myAppPrefsManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = this;
        setContentView(R.layout.activity_pick_location);
        init_views();
        init_events();
        init_activity();

    }
    private void init_views() {

        //Button
        btn_skip = findViewById(R.id.skip);
        //Floating Action Button
        btn_location = findViewById(R.id.location_btn);
        //MyAppPrefManager
        myAppPrefsManager = new MyAppPrefsManager(PickLocationActivity.this);
        tvSelectedAddress = (AutoCompleteTextView) findViewById(R.id.tvSelectedAddress);


    }

    private void init_events() {

        btn_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!currentLatitude.equals("")&&!currentLatitude.equals("")){
                    Intent intent = new Intent(PickLocationActivity.this,SelectStoreActivity.class);
                    if (getIntent().getStringExtra(ConstantValues.PRODUCT_ID)!=null){
                        if (!getIntent().getStringExtra(ConstantValues.PRODUCT_ID).equals("")){
                            intent.putExtra(ConstantValues.PRODUCT_ID,getIntent().getStringExtra(ConstantValues.PRODUCT_ID));
                        }
                    }
                    if (getIntent().getStringExtra(ConstantValues.STORE_ID)!=null){
                        if (!getIntent().getStringExtra(ConstantValues.STORE_ID).equals("")){
                            intent.putExtra(ConstantValues.STORE_ID,getIntent().getStringExtra(ConstantValues.STORE_ID));
                        }
                    }
                    myAppPrefsManager.setMyLat(currentLatitude);
                    myAppPrefsManager.setMyLng(currentLongitude);
                    //intent.putExtra("lat",currentLatitude);
                    //intent.putExtra("lng",currentLongitude);
                    startActivity(intent);
                    finish();
                }else {
                    Toast.makeText(PickLocationActivity.this, getResources().getString(R.string.no_location_selected), Toast.LENGTH_SHORT).show();
                }
            }
        });
        btn_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(PickLocationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(PickLocationActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if (currentLocation !=null){
                    currentLatitude = String.valueOf(currentLocation.getLatitude());
                    currentLongitude = String.valueOf(currentLocation.getLongitude());
                    //map.addMarker(new MarkerOptions().position(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude())).title(getResources().getString(R.string.map_my_location))).setDraggable(true);
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),15.0f));
                }
            }
        });
    }

    private void init_activity() {
        initMap();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);


        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS: {
                        if (ActivityCompat.checkSelfPermission(PickLocationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(PickLocationActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        if (currentLocation != null) {
                            currentLatitude = String.valueOf(currentLocation.getLatitude());
                            currentLongitude = String.valueOf(currentLocation.getLongitude());
                            //map.addMarker(new MarkerOptions().position(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude())).title(getResources().getString(R.string.map_my_location))).setDraggable(true);

                            marker = mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()))
                                    .title("Location")
                                    .icon(BitmapDescriptorFactory
                                            .defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                                //Move Camera to this location
                                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()),15.0f));

                        }
                    }
                    break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    PickLocationActivity.this,
                                    REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        if (isGPSOn()){
            mGoogleApiClient = new GoogleApiClient.Builder(PickLocationActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(PickLocationActivity.this)
                    .addOnConnectionFailedListener(PickLocationActivity.this).build();
            mGoogleApiClient.connect();
        }else {
            btn_location.setVisibility(View.GONE);
        }
        this.mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                    currentLatitude = String.valueOf(latLng.latitude);
                    currentLongitude = String.valueOf(latLng.longitude);
                    if (marker != null) {
                        marker.remove();
                        marker = mMap.addMarker(new MarkerOptions()
                                .position(latLng)
                                .icon(BitmapDescriptorFactory
                                        .defaultMarker(BitmapDescriptorFactory.HUE_RED))
                                .title("Location"));
                    } else {
                        marker = mMap.addMarker(new MarkerOptions()
                                .position(latLng)
                                .icon(BitmapDescriptorFactory
                                        .defaultMarker(BitmapDescriptorFactory.HUE_RED))
                                .title("Location"));
                    }
            }
        });

    }
   // ActivityPickLocationBinding binding;
    private void initMap(){
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        tvSelectedAddress.setHint("Search for Address");


//
//        tvSelectedAddress.setAdapter(new GooglePlacesAutocompleteAdapter(mActivity.getApplicationContext(), R.layout.autocomplete_list_item));
//        tvSelectedAddress.setOnItemClickListener((AdapterView.OnItemClickListener) this);

        tvSelectedAddress.setAdapter(new GooglePlacesAutocompleteAdapter(mActivity.getApplicationContext(), R.layout.autocomplete_list_item));
        tvSelectedAddress.setOnItemClickListener(this);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        Log.d("onActivityResult()", Integer.toString(resultCode));

        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        // All required changes were successfully made
                        Toast.makeText(PickLocationActivity.this, getResources().getString(R.string.gps_on), Toast.LENGTH_LONG).show();
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        if (currentLocation !=null){
                            currentLatitude = String.valueOf(currentLocation.getLatitude());
                            currentLongitude = String.valueOf(currentLocation.getLongitude());
                            marker = mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()))
                                    .title("Location")
                                    .icon(BitmapDescriptorFactory
                                            .defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                            //Move Camera to this location
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()),15.0f));


                            mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                                @Override
                                public void onCameraIdle() {
                                    //get latlng at the center by calling
                                    LatLng midLatLng = mMap.getCameraPosition().target;
                                    Geocoder geocoder;
//                AddressLatLng.lat = midLatLng.latitude;
//                AddressLatLng.lng = midLatLng.longitude;
                                    List<Address> addressList = null;
                                    String addressline1 = "";
                                    geocoder = new Geocoder( PickLocationActivity.this, Locale.getDefault());
                                    mCurrentLattitude = latitude;
                                    mCurrentLongitude = longitude;
                                    MyAppPrefsManager.LocationProperties.setLatitude(String.valueOf(mCurrentLattitude),PickLocationActivity.this.getApplicationContext());
                                    MyAppPrefsManager.LocationProperties.setLongitude(String.valueOf(mCurrentLongitude), PickLocationActivity.this.getApplicationContext());

                                    try {
                                        addressList = geocoder.getFromLocation(midLatLng.latitude, midLatLng.longitude, 1);

                    /*content.startRippleAnimation();
                    ivLocation.startAnimation(AnimationUtils.loadAnimation(MapActivity.this, R.anim.wave_scale));*/
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    if (addressList != null && addressList.size() > 0) {

                                        tvSelectedAddress.setText(addressList.get(0).getAddressLine(0));
                                    }

                                }
                                                            });

                        }
                        break;
                    }
                    case Activity.RESULT_CANCELED:
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(PickLocationActivity.this);
                        builder.setMessage(getResources().getString(R.string.gps_off)).setCancelable(false).
                                setPositiveButton(getResources().getString(R.string.no_internet_ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        finish();
                                    }
                                }).show();
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
                break;
        }
    }


    private void positionToAddress(LatLng point) throws IOException {

        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        addresses = geocoder.getFromLocation(point.latitude, point.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName();

        String completeAddress = address + " " + city + " " + state + " " + country + " " + postalCode;

        if (completeAddress != null)
            tvSelectedAddress.setText(completeAddress);
    }

    private void moveMap() {

        LatLng latLng = new LatLng(mCurrentLattitude, mCurrentLongitude);
        /*Marker s = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(false)
        );*/

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
        mMap.getUiSettings().setZoomControlsEnabled(false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);

    }

    @Override
    public void onClick(View v) {

            if (!tvSelectedAddress.getText().toString().isEmpty()) {
                Bundle bundle = new Bundle();
                bundle.putString("SELECEDADDRESS", tvSelectedAddress.getText().toString());
//                bundle.putString("address_key", mAddressKey);
//                bundle.putString("address_type", address_type);
//                bundle.putString("address_type_name", address_type_name);
//                bundle.putString("streetName", streetName);
//                bundle.putString("company", company);
//                bundle.putString("landmark", landmark);
//                bundle.putString("city_key", city_key);
//                bundle.putString("city_name", city_name);
//                bundle.putString("area_key", area_key);
//                bundle.putString("area_name", area_name);
                bundle.putString("latitude", String.valueOf(latitude));
                bundle.putString("longitude", String.valueOf(longitude));
                //bundle.putString("apartment", apartment);
                //bundle.putString("flatno", flatno);
                 }



    }
    private class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {

        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context mContext, int searchplace_list) {
            super(mContext, searchplace_list);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }


        public ArrayList<String> autocomplete(String input) {
            ArrayList<String> resultList = null;


            HttpURLConnection conn = null;
            StringBuilder jsonResults = new StringBuilder();
            try {
                StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
                sb.append("?key=" + getResources().getString(R.string.google_api_app_id));

                //sb.append("?key=AIzaSyDAF9hsFi2p4qyU8dlvmltkp-EF1uY4Zc4");


                sb.append("&components=");
                sb.append("&input=" + URLEncoder.encode(input, "utf8"));


                URL url = new URL(sb.toString());

                System.out.println("URL: " + url);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(conn.getInputStream());

                // Load the results into a StringBuilder
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
            } catch (MalformedURLException e) {
                return resultList;
            } catch (IOException e) {
                return resultList;
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }

            try {

                // Create a JSON object hierarchy from the results
                JSONObject jsonObj = new JSONObject(jsonResults.toString());
                JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

                // Extract the Place descriptions from the results
                resultList = new ArrayList<String>(predsJsonArray.length());
                for (int i = 0; i < predsJsonArray.length(); i++) {
                    System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                    //  System.out.println("lat :" + predsJsonArray.getJSONObject(i).getString("lat"));
                    System.out.println("============================================================");
                    // resultList.add(predsJsonArray.getJSONArray(i));
                    resultList.add(predsJsonArray.getJSONObject(i).getString("description"));

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return resultList;

        }
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        str = (String) parent.getItemAtPosition(position);
        Geocoder coder = new Geocoder(this);
        try {
            ArrayList<Address> adresses = (ArrayList<Address>) coder.getFromLocationName(str, 10);
            for (Address add : adresses) {
                Double longitude = add.getLongitude();
                Double latitude = add.getLatitude();
                MyAppPrefsManager.LocationProperties.setLatitude(String.valueOf(add.getLatitude()), this.getApplicationContext());
                MyAppPrefsManager.LocationProperties.setLongitude(String.valueOf(add.getLongitude()),this.getApplicationContext());
                System.out.println(longitude + "  " + latitude);
                MyAppPrefsManager.hideSoftKeyboard(this);
                LatLng mLatLng = new LatLng(latitude, longitude);
                /*mMarker = googleMap.addMarker(new MarkerOptions().position(mLatLng)
                        .draggable(true));
                mMarker.setTag(9999);*/

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(mLatLng)      // Sets the center of the map to location user
                        .zoom(17)                   // Sets the zoom
                        .build();                   // Creates a CameraPosition from the builder
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            }
        } catch (IOException | IllegalArgumentException e) {
            e.printStackTrace();
        }
    }


    private boolean isGPSOn(){
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            return false;
        }else {
            return true;
        }
    }
}


