package com.zomart.app.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.paypal.android.sdk.df;
import com.zomart.app.R;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import com.zomart.app.activities.MainActivity;
import com.zomart.app.adapters.ShippingMethodsAdapter;
import com.zomart.app.app.App;
import com.zomart.app.constant.ConstantValues;
import com.zomart.app.customs.CustomEditText;
import com.zomart.app.customs.DialogLoader;
import com.zomart.app.databases.User_Cart_DB;
import com.zomart.app.models.api_response_model.ErrorResponse;
import com.zomart.app.models.cart_model.CartDetails;
import com.zomart.app.models.location_model.Countries;
import com.zomart.app.models.location_model.Zones;
import com.zomart.app.models.order_model.OrderDetails;
import com.zomart.app.models.order_model.OrderShippingMethod;
import com.zomart.app.models.shipping_model.ShippingMethods;
import com.zomart.app.models.shipping_model.ShippingZone;
import com.zomart.app.models.shipping_model.ShippingZoneLocations;
import com.zomart.app.models.user_model.UserBilling;
import com.zomart.app.models.user_model.UserData;
import com.zomart.app.models.user_model.UserDetails;
import com.zomart.app.models.user_model.UserShipping;
import com.zomart.app.network.APIClient;
import com.zomart.app.utils.LocationHelper;
import com.zomart.app.utils.ValidateInputs;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;


public class Shipping_Address extends Fragment {
    
    View rootView;
    String customerID,GetArgument;
    
    Zones selectedZone;
    Countries selectedCountry;
    DialogLoader dialogLoader;
    UserShipping userShipping;
    LocationHelper locationHelper;
    
    List<Zones> zoneList;
    List<Countries> countryList;
    
    List<String> zoneNames;
    List<String> countryNames;
    ArrayAdapter<String> zoneAdapter;
    ArrayAdapter<String> countryAdapter;
    
    Button proceed_checkout_btn;
    LinearLayout default_shipping_layout;
    CustomEditText input_firstname, input_lastname, input_address_1, input_address_2, input_company;
    CustomEditText input_country, input_zone, input_city, input_postcode, input_email, input_contact;

    //For shipping method-----------------------------------------------------------------------------
    List<ShippingMethods> shippingMethodsList;

    List<ShippingZone> shippingZones;
    List<ShippingZoneLocations> shippingZoneLocations;

    UserShipping userShipping1;
    User_Cart_DB user_cart_db;
    DialogLoader dialogLoader1;

    public ShippingMethods selectedShippingMethod = null;
    //------------------------------------------------------------------------------------------------
    
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.address, container, false);
        
        // Set the Title of Toolbar
        try {
            //((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.shipping_address));
            ((MainActivity)getActivity()).setMyTitle(getString(R.string.shipping_address));
        }catch (Exception e){}
        
        // Get the customersID and defaultAddressID from SharedPreferences
        customerID = this.getContext().getSharedPreferences("UserInfo", getContext().MODE_PRIVATE).getString("userID", "");
        
        
        // Binding Layout Views
        input_firstname = (CustomEditText) rootView.findViewById(R.id.firstname);
        //input_firstname.setSelection(input_firstname.getText().length());
        input_lastname = (CustomEditText) rootView.findViewById(R.id.lastname);
        input_address_1 = (CustomEditText) rootView.findViewById(R.id.address_1);
        input_address_2 = (CustomEditText) rootView.findViewById(R.id.address_2);
        input_company = (CustomEditText) rootView.findViewById(R.id.company);
        input_country = (CustomEditText) rootView.findViewById(R.id.country);
        input_zone = (CustomEditText) rootView.findViewById(R.id.zone);
        input_city = (CustomEditText) rootView.findViewById(R.id.city);
        input_postcode = (CustomEditText) rootView.findViewById(R.id.postcode);
        input_email = (CustomEditText) rootView.findViewById(R.id.email);
        input_contact = (CustomEditText) rootView.findViewById(R.id.contact);
        proceed_checkout_btn = (Button) rootView.findViewById(R.id.save_address_btn);
        default_shipping_layout = (LinearLayout) rootView.findViewById(R.id.default_shipping_layout);
        
        
        // Set KeyListener of some View to null
        input_country.setKeyListener(null);
        input_zone.setKeyListener(null);
        input_zone.setFocusableInTouchMode(false);
        
        // Hide the Default Checkbox Layout
        input_email.setVisibility(View.GONE);
        input_contact.setVisibility(View.GONE);
        default_shipping_layout.setVisibility(View.GONE);
        
        // Set the text of Button
        
        if(getArguments()!=null){
            GetArgument = getArguments().getString("shipping");
            if("1".equalsIgnoreCase(GetArgument)){
                // Enable Drawer Indicator with static variable actionBarDrawerToggle of MainActivity
               // MainActivity.actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
                proceed_checkout_btn.setText(getContext().getString(R.string.update));
            }
            else {
                proceed_checkout_btn.setText(getContext().getString(R.string.next));
            }
        }
        
        
        
        dialogLoader = new DialogLoader(getContext());
        locationHelper = new LocationHelper(getContext());
        
        userShipping = ((App) getContext().getApplicationContext()).getOrderDetails().getShipping();
        
        
        zoneList = new ArrayList<>();
        zoneNames = new ArrayList<>();
        countryNames = new ArrayList<>();
        
        selectedZone = null;
        selectedCountry = null;
        
        
        countryList = locationHelper.getCountries();
        
        for (int i=0;  i<countryList.size();  i++) {
            countryNames.add(countryList.get(i).getName());
        }
        countryNames.add("Other");
        
        
        
        if (userShipping == null) {
            // Request User Info
            if (!ConstantValues.IS_GUEST_LOGGED_IN)
                RequestUserDetails();
        }
        else {
            // Set the Address details
            input_firstname.setText(userShipping.getFirstName());
            input_lastname.setText(userShipping.getLastName());
            input_address_1.setText(userShipping.getAddress1());
            input_address_2.setText(userShipping.getAddress2());
            input_company.setText(userShipping.getCompany());
            input_city.setText(userShipping.getCity());
            input_postcode.setText(userShipping.getPostcode());
            
            String zone_code = "";
            String country_code = "";
            
            if (!TextUtils.isEmpty(userShipping.getCountry())) {
                country_code = userShipping.getCountry();
                selectedCountry = locationHelper.getCountryFromCode(country_code);
                //todo change country here
                //input_country.setText(selectedCountry.getName());
                input_country.setText("Lebanon");
                
                // Get Zones of the selected Country
                zoneList.clear();
                zoneList = new ArrayList<>();
                //todo change country here
                //zoneList = locationHelper.getStates(selectedCountry.getValue());
                zoneList = locationHelper.getStates("LB");
                
                zoneNames.clear();
                
                // Add the Zone Names to the zoneNames List
                for (int x=0;  x<zoneList.size();  x++){
                    zoneNames.add(zoneList.get(x).getName());
                }
                zoneNames.add("Other");
                
                input_zone.setFocusableInTouchMode(true);
            }
            
            
            if (!TextUtils.isEmpty(userShipping.getState())) {
                zone_code = userShipping.getState();
                selectedZone = locationHelper.getStateFromCode(country_code, zone_code);
                input_zone.setText(selectedZone.getName());
            }
        }
        
        
        // Handle Touch event of input_country EditText
        input_country.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    
                    countryAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1);
                    countryAdapter.addAll(countryNames);
                    
                    AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                    View dialogView = getActivity().getLayoutInflater().inflate(R.layout.dialog_list_search, null);
                    dialog.setView(dialogView);
                    dialog.setCancelable(false);
                    
                    Button dialog_button = (Button) dialogView.findViewById(R.id.dialog_button);
                    EditText dialog_input = (EditText) dialogView.findViewById(R.id.dialog_input);
                    TextView dialog_title = (TextView) dialogView.findViewById(R.id.dialog_title);
                    ListView dialog_list = (ListView) dialogView.findViewById(R.id.dialog_list);
                    
                    dialog_title.setText(getString(R.string.country));
                    dialog_list.setVerticalScrollBarEnabled(true);
                    dialog_list.setAdapter(countryAdapter);
                    
                    dialog_input.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                        @Override
                        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                            countryAdapter.getFilter().filter(charSequence);
                        }
                        @Override
                        public void afterTextChanged(Editable s) {}
                    });
                    
                    
                    final AlertDialog alertDialog = dialog.create();
                    
                    dialog_button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });
                    
                    alertDialog.show();
                    
                    dialog_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            
                            final String selectedItem = countryAdapter.getItem(position);
                            input_country.setText(selectedItem);
                            
                            if (!selectedItem.equalsIgnoreCase("Other")) {
                                
                                for (int i=0;  i<countryList.size();  i++) {
                                    if (countryList.get(i).getName().equalsIgnoreCase(selectedItem)) {
                                        
                                        selectedCountry = countryList.get(i);
                                        Log.i("sel_con_val", "onItemClick: "+selectedCountry.getName()+" "+selectedCountry.getValue());
                                        
                                        // Get Zones of the selected Country
                                        zoneList.clear();
                                        zoneList = new ArrayList<>();
                                        //todo change country here
                                        //zoneList = locationHelper.getStates(selectedCountry.getValue());
                                        zoneList = locationHelper.getStates("LB");
                                        
                                        zoneNames.clear();
                                        
                                        // Add the Zone Names to the zoneNames List
                                        for (int x=0;  x<zoneList.size();  x++){
                                            zoneNames.add(zoneList.get(x).getName());
                                        }
                                        zoneNames.add("Other");
                                    }
                                }
                                
                            }
                            else {
                                selectedCountry = null;
                                
                                // Get Zones of the selected Country
                                zoneList.clear();
                                zoneList = new ArrayList<>();
                                
                                zoneNames.clear();
                                zoneNames.add("Other");
                            }
                            
                            input_zone.setText("");
                            input_zone.setFocusableInTouchMode(true);
                            
                            alertDialog.dismiss();
                        }
                    });
                    
                }
                
                return false;
            }
        });
        
        
        // Handle Touch event of input_zone EditText
        input_zone.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    
                    zoneAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1);
                    zoneAdapter.addAll(zoneNames);
                    
                    AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                    View dialogView = getActivity().getLayoutInflater().inflate(R.layout.dialog_list_search, null);
                    dialog.setView(dialogView);
                    dialog.setCancelable(false);
                    
                    Button dialog_button = (Button) dialogView.findViewById(R.id.dialog_button);
                    EditText dialog_input = (EditText) dialogView.findViewById(R.id.dialog_input);
                    TextView dialog_title = (TextView) dialogView.findViewById(R.id.dialog_title);
                    ListView dialog_list = (ListView) dialogView.findViewById(R.id.dialog_list);
                    
                    dialog_title.setText(getString(R.string.zone));
                    dialog_list.setVerticalScrollBarEnabled(true);
                    dialog_list.setAdapter(zoneAdapter);
                    
                    dialog_input.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                        @Override
                        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                            zoneAdapter.getFilter().filter(charSequence);
                        }
                        @Override
                        public void afterTextChanged(Editable s) {}
                    });
                    
                    
                    final AlertDialog alertDialog = dialog.create();
                    
                    dialog_button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });
                    
                    alertDialog.show();
                    
                    
                    
                    dialog_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            
                            final String selectedItem = zoneAdapter.getItem(position);
                            input_zone.setText(selectedItem);
                            
                            if (!zoneAdapter.getItem(position).equalsIgnoreCase("Other")) {
                                
                                for (int i=0;  i<zoneList.size();  i++) {
                                    if (zoneList.get(i).getName().equalsIgnoreCase(selectedItem)) {
                                        
                                        selectedZone = zoneList.get(i);
                                    }
                                }
                            }
                            else {
                                selectedZone = null;
                            }
                            
                            alertDialog.dismiss();
                        }
                    });
                    
                }
                
                return false;
            }
        });
        
        
        // Handle the Click event of Proceed Order Button
        proceed_checkout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
                if ("1".equalsIgnoreCase(GetArgument)) {
                    processUpdateUserForShipping();
                    
                } else {
                    
                    // Validate Address Form Inputs
                    boolean isValidData = validateAddressForm();
                    
                    if (isValidData) {
                        // New Instance of AddressDetails
                        userShipping = new UserShipping();
                        
                        userShipping.setFirstName(input_firstname.getText().toString().trim());
                        userShipping.setLastName(input_lastname.getText().toString().trim());
                        userShipping.setAddress1(input_address_1.getText().toString().trim());
                        userShipping.setAddress2(input_address_2.getText().toString().trim());
                        userShipping.setCompany(input_company.getText().toString().trim());
                        userShipping.setCity(input_city.getText().toString().trim());
                        userShipping.setPostcode(input_postcode.getText().toString().trim());

                        //todo Change country here
                        userShipping.setCountry("LB");
                        /*
                        if (selectedCountry != null) {
                            userShipping.setCountry(selectedCountry.getValue());
                        } else {
                            userShipping.setCountry("");
                        }
                         */

                        //todo change zone here
                        userShipping.setState("");
                        /*
                        if (selectedZone != null) {
                            userShipping.setState(selectedZone.getValue());
                        } else {
                            userShipping.setState("");
                        }
                        **/

                        
                        
                        OrderDetails orderDetails = ((App) getContext().getApplicationContext()).getOrderDetails();
                        orderDetails.setShipping(userShipping);
                        
                        ((App) getContext().getApplicationContext()).setOrderDetails(orderDetails);
                        ((App) getContext().getApplicationContext()).getUserDetails().setShipping(userShipping);

                        //Add billing address as well
                        UserBilling userBilling = new UserBilling();
                        userBilling.setFirstName(input_firstname.getText().toString().trim());
                        userBilling.setLastName(input_lastname.getText().toString().trim());
                        userBilling.setAddress1(input_address_1.getText().toString().trim());
                        userBilling.setAddress2(input_address_2.getText().toString().trim());
                        userBilling.setCompany(input_company.getText().toString().trim());
                        userBilling.setCity(input_city.getText().toString().trim());
                        userBilling.setPostcode(input_postcode.getText().toString().trim());

                        userBilling.setEmail(getContext().getSharedPreferences("UserInfo", getContext().MODE_PRIVATE).getString("userEmail", ""));
                        userBilling.setPhone("00961999999999");

                        //todo Change country here
                        userBilling.setCountry("LB");
                        /*
                        if (selectedCountry != null) {
                            userBilling.setCountry(selectedCountry.getValue());
                        } else {
                            userBilling.setCountry("");
                        }
                        **/

                        //todo change zone here
                        userBilling.setState("");
                        /*
                        if (selectedZone != null) {
                            userBilling.setState(selectedZone.getValue());
                        } else {
                            userBilling.setState("");
                        }
                         */

                        OrderDetails orderDetails2 = ((App) getContext().getApplicationContext()).getOrderDetails();
                        orderDetails2.setBilling(userBilling);
                        orderDetails2.setSameAddress(true);

                        ((App) getContext().getApplicationContext()).setOrderDetails(orderDetails2);
                        ((App) getContext().getApplicationContext()).getUserDetails().setBilling(userBilling);
                        // Navigate to Shipping_Methods Fragment
                        //todo it was navigated to shipping methods but now to checkout

                        processShippingMethod();
                        /*
                        Fragment fragment = new Shipping_Methods();
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.main_fragment, fragment)
                                .addToBackStack(null).commit();

                         */
                        /*
                        Bundle bundle = new Bundle();
                        bundle.putString("shipping", "0");
                        
                        // Navigate to Billing_Address Fragment
                        Fragment fragment = new Billing_Address();
                        fragment.setArguments(bundle);
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.main_fragment, fragment)
                                .addToBackStack(null).commit();

                         */
                        
                    }
                }
            }
        });
        
        
        return rootView;
    }

    private void processShippingMethod(){
        // Get the Shipping Address from AppContext
        userShipping1 = ((App) getContext().getApplicationContext()).getOrderDetails().getShipping();
        List<OrderShippingMethod> orderShippingMethods = ((App) getContext().getApplicationContext()).getOrderDetails().getOrderShippingMethods();

        if (orderShippingMethods.size() > 0) {
            selectedShippingMethod = new ShippingMethods();
            selectedShippingMethod.setId(orderShippingMethods.get(0).getShippingID());
            selectedShippingMethod.setMethodId(orderShippingMethods.get(0).getMethodId());
            selectedShippingMethod.setMethodTitle(orderShippingMethods.get(0).getMethodTitle());
            selectedShippingMethod.setTitle(orderShippingMethods.get(0).getTotal());
        }

        user_cart_db = new User_Cart_DB();
        dialogLoader1 = new DialogLoader(getContext());
        shippingZones = new ArrayList<>();
        shippingZoneLocations = new ArrayList<>();
        shippingMethodsList = new ArrayList<>();

        // Request Shipping Rates
        GetShippingZonesAndLocations shippingZonesAndLocations = new GetShippingZonesAndLocations();
        shippingZonesAndLocations.execute();
    }
//*********** Proceed Update of User's Details ********//
    
    private void processUpdateUserForShipping() {
        UserDetails user = new UserDetails();
        UserShipping shippingMethodsApi = new UserShipping();
        
        // Validate Address Form Inputs
        boolean isValidData = validateAddressForm();
        
        if (isValidData) {
            // New Instance of AddressDetails
            
            shippingMethodsApi.setFirstName(input_firstname.getText().toString().trim());
            shippingMethodsApi.setLastName(input_lastname.getText().toString().trim());
            shippingMethodsApi.setAddress1(input_address_1.getText().toString().trim());
            shippingMethodsApi.setAddress2(input_address_2.getText().toString().trim());
            shippingMethodsApi.setCompany(input_company.getText().toString().trim());
            shippingMethodsApi.setCity(input_city.getText().toString().trim());
            shippingMethodsApi.setPostcode(input_postcode.getText().toString().trim());

            //todo Change country here
            shippingMethodsApi.setCountry("LB");
            /*
            if (selectedCountry != null) {
                shippingMethodsApi.setCountry(selectedCountry.getValue());
            } else {
                shippingMethodsApi.setCountry("");
            }
            **/

            //todo change zone here
            shippingMethodsApi.setState("");
            /*
            if (selectedZone != null) {
                shippingMethodsApi.setState(selectedZone.getValue());
            } else {
                shippingMethodsApi.setState("");
            }
            **/
        }
        
        user.setShipping(shippingMethodsApi);
        updateCustomerInfoForShipping(user);
    }

    private void processUpdateUserForBilling() {
        UserDetails user = new UserDetails();
        UserBilling shippingMethodsApi = new UserBilling();

        shippingMethodsApi.setFirstName(input_firstname.getText().toString().trim());
        shippingMethodsApi.setLastName(input_lastname.getText().toString().trim());
        shippingMethodsApi.setAddress1(input_address_1.getText().toString().trim());
        shippingMethodsApi.setAddress2(input_address_2.getText().toString().trim());
        shippingMethodsApi.setCompany(input_company.getText().toString().trim());
        shippingMethodsApi.setCity(input_city.getText().toString().trim());
        shippingMethodsApi.setPostcode(input_postcode.getText().toString().trim());
        shippingMethodsApi.setEmail(getContext().getSharedPreferences("UserInfo", getContext().MODE_PRIVATE).getString("userEmail", ""));
        shippingMethodsApi.setPhone("00961999999999");

        //todo change country here
        shippingMethodsApi.setCountry("LB");
        /*
        if (selectedCountry != null) {
            shippingMethodsApi.setCountry(selectedCountry.getValue());
        } else {
            shippingMethodsApi.setCountry("");
        }
        **/

        //todo change zone here
        shippingMethodsApi.setState("");
        /*
        if (selectedZone != null) {
            shippingMethodsApi.setState(selectedZone.getValue());
        } else {
            shippingMethodsApi.setState("");
        }
        **/

        user.setBilling(shippingMethodsApi);
        updateCustomerInfoForBilling(user);
    }
    
    
    //*********** Updates User's Personal Information ********//
    
    private void updateCustomerInfoForShipping(UserDetails userDetails) {
        
        dialogLoader.showProgressDialog();
    
        String json = new Gson().toJson(userDetails);
        Call<UserDetails> call = APIClient.getInstance()
                .updateCustomerAddress
                        (       customerID,
                                userDetails
                        );
        
        call.enqueue(new Callback<UserDetails>() {
            @Override
            public void onResponse(Call<UserDetails> call, retrofit2.Response<UserDetails> response) {
                
                dialogLoader.hideProgressDialog();
                String url = response.raw().request().url().toString();
                // Check if the Response is successful
                if (response.isSuccessful()) {
                    String url2 = response.raw().request().url().toString();
                    String str = new Gson().toJson(response);
                    Log.d("Response",str);
                    
                    // New Instance of AddressDetails
                    userShipping = new UserShipping();
                    
                    userShipping.setFirstName(input_firstname.getText().toString().trim());
                    userShipping.setLastName(input_lastname.getText().toString().trim());
                    userShipping.setAddress1(input_address_1.getText().toString().trim());
                    userShipping.setAddress2(input_address_2.getText().toString().trim());
                    userShipping.setCompany(input_company.getText().toString().trim());
                    userShipping.setCity(input_city.getText().toString().trim());
                    userShipping.setPostcode(input_postcode.getText().toString().trim());

                    //todo change country here
                    userShipping.setCountry("LB");
                    /*
                    if (selectedCountry != null) {
                        userShipping.setCountry(selectedCountry.getValue());
                    } else {
                        userShipping.setCountry("");
                    }
                    **/

                    //todo change zone here
                    userShipping.setState("");
                    /*
                    if (selectedZone != null) {
                        userShipping.setState(selectedZone.getValue());
                    } else {
                        userShipping.setState("");
                    }
                    **/
                    
                    
                    OrderDetails orderDetails = ((App) getContext().getApplicationContext()).getOrderDetails();
                    orderDetails.setShipping(userShipping);
                    
                    ((App) getContext().getApplicationContext()).setOrderDetails(orderDetails);
                    ((App) getContext().getApplicationContext()).getUserDetails().setShipping(userShipping);
                    
                    
                    Snackbar.make(rootView, getString(R.string.account_updated), Snackbar.LENGTH_SHORT).show();

                    processUpdateUserForBilling();

                    /*
                    Bundle bundle = new Bundle();
                    bundle.putString("shipping", "1");
                    // Navigate to Billing_Address Fragment
                    Fragment fragment = new Billing_Address();
                    fragment.setArguments(bundle);
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.main_fragment, fragment)
                            .addToBackStack(null).commit();

                     */
                    
                }
                
                else {
                    
                    Converter<ResponseBody, UserData> converter = APIClient.retrofit.responseBodyConverter(UserData.class, new java.lang.annotation.Annotation[0]);
                    UserData userData;
                    try {
                        userData = converter.convert(response.errorBody());
                    } catch (IOException e) {
                        userData = new UserData();
                    }
                    
                    Toast.makeText(App.getContext(), "Error : "+userData.getError(), Toast.LENGTH_SHORT).show();
                }
            }
            
            @Override
            public void onFailure(Call<UserDetails> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getContext(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void updateCustomerInfoForBilling(UserDetails userDetails) {

        dialogLoader.showProgressDialog();

        Call<UserDetails> call = APIClient.getInstance()
                .updateCustomerAddress
                        (       customerID,
                                userDetails
                        );

        call.enqueue(new Callback<UserDetails>() {
            @Override
            public void onResponse(Call<UserDetails> call, retrofit2.Response<UserDetails> response) {

                dialogLoader.hideProgressDialog();

                // Check if the Response is successful
                if (response.isSuccessful()) {

                    String str = new Gson().toJson(response);
                    Log.d("Response",str);

                    //Snackbar.make(rootView, getString(R.string.account_updated), Snackbar.LENGTH_SHORT).show();

                    FragmentManager fragmentManager = ((MainActivity) getContext()).getSupportFragmentManager();
                    fragmentManager.popBackStack();
                    /*
                    // Navigate to Home Fragment
                    ((MainActivity) getContext()).setupExpandableDrawerList();
                    ((MainActivity) getContext()).setupExpandableDrawerHeader();
                    // Navigate to HomePage Fragment
                    if (getFragmentManager().getBackStackEntryCount() > 0) {
                        getFragmentManager().popBackStack(getString(R.string.actionHome), FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                     */

                }

                else {

                    Converter<ResponseBody, UserData> converter = APIClient.retrofit.responseBodyConverter(UserData.class, new java.lang.annotation.Annotation[0]);
                    UserData userData;
                    try {
                        userData = converter.convert(response.errorBody());
                    } catch (IOException e) {
                        userData = new UserData();
                    }

                    Toast.makeText(App.getContext(), "Error : "+userData.getError(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserDetails> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getContext(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }
    
    
    //*********** Request User's Info form Server ********//
    
    public void RequestUserDetails() {
        
        dialogLoader.showProgressDialog();
        
        Call<UserDetails> call = APIClient.getInstance()
                .getUserInfo
                        (
                                customerID
                        );
        
        call.enqueue(new Callback<UserDetails>() {
            @Override
            public void onResponse(Call<UserDetails> call, retrofit2.Response<UserDetails> response) {
                
                dialogLoader.hideProgressDialog();
                
                // Check if the Response is successful
                if (response.isSuccessful()) {
                    if (response.body().getShipping() != null) {
                        
                        userShipping = response.body().getShipping();
                        
                        // Set the Address details
                        input_firstname.setText(userShipping.getFirstName());
                        input_lastname.setText(userShipping.getLastName());
                        input_address_1.setText(userShipping.getAddress1());
                        input_address_2.setText(userShipping.getAddress2());
                        input_company.setText(userShipping.getCompany());
                        input_city.setText(userShipping.getCity());
                        input_postcode.setText(userShipping.getPostcode());
                        
                        String zone_code = "";
                        String country_code = "";
                        
                        if (!TextUtils.isEmpty(userShipping.getCountry())) {
                            country_code = userShipping.getCountry();
                            selectedCountry = locationHelper.getCountryFromCode(country_code);
                            //todo change country here
                            //input_country.setText(selectedCountry.getName());
                            input_country.setText("Lebanon");
                            
                            // Get Zones of the selected Country
                            zoneList.clear();
                            zoneList = new ArrayList<>();
                            //todo change Country here
                            //zoneList = locationHelper.getStates(selectedCountry.getValue());
                            zoneList = locationHelper.getStates("LB");
                            
                            zoneNames.clear();
                            
                            // Add the Zone Names to the zoneNames List
                            for (int x=0;  x<zoneList.size();  x++){
                                zoneNames.add(zoneList.get(x).getName());
                            }
                            zoneNames.add("Other");
                            
                            input_zone.setFocusableInTouchMode(true);
                        }
                        
                        
                        if (!TextUtils.isEmpty(userShipping.getState())) {
                            zone_code = userShipping.getState();
                            selectedZone = locationHelper.getStateFromCode(country_code, zone_code);
                            //todo change zone here
                            //input_zone.setText(selectedZone.getName());
                            input_zone.setText("Other");
                        }
                    }
                    else {
                        // Set the Address details
                        input_firstname.setText("");
                        input_lastname.setText("");
                        input_address_1.setText("");
                        input_address_2.setText("");
                        input_company.setText("");
                        input_country.setText("");
                        input_zone.setText("");
                        input_city.setText("");
                        input_postcode.setText("");
                        
                        String zone_code = "";
                        String country_code = "";
                        
                        selectedCountry = null;
                        selectedZone = null;
                    }
                }
                else {
                    // Set the Address details
                    input_firstname.setText("");
                    input_lastname.setText("");
                    input_address_1.setText("");
                    input_address_2.setText("");
                    input_company.setText("");
                    input_country.setText("");
                    input_zone.setText("");
                    input_city.setText("");
                    input_postcode.setText("");
                    
                    String zone_code = "";
                    String country_code = "";
                    
                    selectedCountry = null;
                    selectedZone = null;
                }
            }
            
            @Override
            public void onFailure(Call<UserDetails> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getContext(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }
    
    
    
    //*********** Validate Address Form Inputs ********//
    
    private boolean validateAddressForm() {
        if (!ValidateInputs.isValidName(input_firstname.getText().toString().trim())) {
            input_firstname.setError(getString(R.string.invalid_first_name));
            return false;
        } else if (!ValidateInputs.isValidName(input_lastname.getText().toString().trim())) {
            input_lastname.setError(getString(R.string.invalid_last_name));
            return false;
        } else if (!ValidateInputs.isValidInput(input_address_1.getText().toString().trim())) {
            input_address_1.setError(getString(R.string.invalid_address));
            return false;
        } else if (!ValidateInputs.isIfValidInput(input_address_2.getText().toString().trim())) {
            input_address_2.setError(getString(R.string.invalid_address));
            return false;
        } else if (!ValidateInputs.isIfValidInput(input_company.getText().toString().trim())) {
            input_company.setError(getString(R.string.invalid_company));
            return false;
        }
        //else if (!ValidateInputs.isValidInput(input_country.getText().toString().trim())) {
          //  input_country.setError(getString(R.string.select_country));
            //return false;
       // } else if (!ValidateInputs.isValidInput(input_zone.getText().toString().trim())) {
         //   input_zone.setError(getString(R.string.select_zone));
           // return false;
        //}
        else if (!ValidateInputs.isValidInput(input_city.getText().toString().trim())) {
            input_city.setError(getString(R.string.enter_city));
            return false;
        } else if (!ValidateInputs.isValidNumber(input_postcode.getText().toString().trim())) {
            input_postcode.setError(getString(R.string.invalid_post_code));
            return false;
        } else {
            return true;
        }
    }



    private class GetShippingZonesAndLocations extends AsyncTask<String, Void, String> {

        //*********** Runs on the UI thread before #doInBackground() ********//

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogLoader.showProgressDialog();
        }


        //*********** Performs some Processes on Background Thread and Returns a specified Result  ********//

        @Override
        protected String doInBackground(String... params) {

            RequestShippingZones();

            return "All Done!";
        }


        //*********** Runs on the UI thread after #doInBackground() ********//

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialogLoader.hideProgressDialog();
            matchShippingZone();
        }
    }

    public void RequestShippingZones() {

        Call<List<ShippingZone>> call = APIClient.getInstance()
                .getShippingZones();

        try {
            Response<List<ShippingZone>> response = call.execute();

            if (response.isSuccessful()) {

                shippingZones.addAll(response.body());

                for (int i=0;  i<shippingZones.size();  i++) {
                    RequestShippingZoneLocations(shippingZones.get(i).getId());
                }

            }
            else {
                Converter<ResponseBody, ErrorResponse> converter = APIClient.retrofit.responseBodyConverter(ErrorResponse.class, new Annotation[0]);
                ErrorResponse error;
                try {
                    error = converter.convert(response.errorBody());
                } catch (IOException e) {
                    error = new ErrorResponse();
                }
//                Toast.makeText(getContext(), "Error Fetching Product : "+error, Toast.LENGTH_LONG).show();

            }

        } catch (IOException e) {
            e.printStackTrace();

//            Toast.makeText(getContext(), "Exception : "+e, Toast.LENGTH_LONG).show();
        }
    }

    public void RequestShippingZoneLocations(int zone_id) {

        Call<List<ShippingZoneLocations>> call = APIClient.getInstance()
                .getShippingZoneLocations
                        (
                                String.valueOf(zone_id)
                        );

        try {
            Response<List<ShippingZoneLocations>> response = call.execute();

            if (response.isSuccessful()) {

                List<ShippingZoneLocations> shippingZoneLocationsList = response.body();

                for (int i=0;  i<shippingZoneLocationsList.size();  i++) {

                    shippingZoneLocationsList.get(i).setZoneID(zone_id);
                    shippingZoneLocations.add(shippingZoneLocationsList.get(i));
                }

            }
            else {
                Converter<ResponseBody, ErrorResponse> converter = APIClient.retrofit.responseBodyConverter(ErrorResponse.class, new Annotation[0]);
                ErrorResponse error;
                try {
                    error = converter.convert(response.errorBody());
                } catch (IOException e) {
                    error = new ErrorResponse();
                }
//                Toast.makeText(getContext(), "Error Fetching Product : "+error, Toast.LENGTH_LONG).show();
            }

        } catch (IOException e) {
            e.printStackTrace();
//            Toast.makeText(getContext(), "Exception : "+e, Toast.LENGTH_LONG).show();
        }
    }

    private void matchShippingZone() {
        int zone_ID = 0;
        boolean isMatched = false;

        for (int i=0;  i<shippingZoneLocations.size();  i++) {
            ShippingZoneLocations shippingZoneLocation = shippingZoneLocations.get(i);
            if ("postcode".equalsIgnoreCase(shippingZoneLocation.getType())) {
                if (matchPostCode(shippingZoneLocation)) {
                    zone_ID = shippingZoneLocation.getZoneID();
                    isMatched = true;
                    break;
                }
            }
        }

        if (!isMatched) {
            for (int i=0;  i<shippingZoneLocations.size();  i++) {
                ShippingZoneLocations shippingZoneLocation = shippingZoneLocations.get(i);
                if ("state".equalsIgnoreCase(shippingZoneLocation.getType())) {
                    if ((userShipping.getCountry() +":"+ userShipping.getState()).equalsIgnoreCase(shippingZoneLocation.getCode())) {
                        zone_ID = shippingZoneLocation.getZoneID();
                        isMatched = true;
                        break;
                    }
                }
            }
        }

        if (!isMatched) {
            for (int i=0;  i<shippingZoneLocations.size();  i++) {
                ShippingZoneLocations shippingZoneLocation = shippingZoneLocations.get(i);
                if ("country".equalsIgnoreCase(shippingZoneLocation.getType())) {
                    if (userShipping.getCountry().equalsIgnoreCase(shippingZoneLocation.getCode())) {
                        zone_ID = shippingZoneLocation.getZoneID();
                        isMatched = true;
                        break;
                    }
                }
            }
        }

        if (!isMatched) {
            for (int i=0;  i<shippingZoneLocations.size();  i++) {
                ShippingZoneLocations shippingZoneLocation = shippingZoneLocations.get(i);
                if ("continent".equalsIgnoreCase(shippingZoneLocation.getType())) {
                    LocationHelper locationHelper = new LocationHelper(getContext());
                    String selectedContinent = locationHelper.getContinentCode(userShipping.getCountry());

                    if (selectedContinent.equalsIgnoreCase(shippingZoneLocation.getCode())) {
                        zone_ID = shippingZoneLocation.getZoneID();
                        isMatched = true;
                        break;
                    }
                }
            }
        }


        if (isMatched) {
            RequestShippingMethods(zone_ID);
        }
        else {
//            RequestDefaultShippingMethods();
        }

    }

    private boolean matchPostCode(ShippingZoneLocations shippingZoneLocation) {
        boolean isMatched = false;

        String selectedPostCode = userShipping.getPostcode();

        if (selectedPostCode.equalsIgnoreCase(shippingZoneLocation.getCode()))
            return true;

        if (shippingZoneLocation.getCode().contains("*")) {
            String code = shippingZoneLocation.getCode().replaceAll("\\*", "");
            if (selectedPostCode.startsWith(code))
                return true;
        }

        if (shippingZoneLocation.getCode().contains(".")) {
            String code = shippingZoneLocation.getCode();
            String[] separated = code.split("\\.");
            String min = separated[0];
            String max = separated[3].trim();

            if (Long.parseLong(selectedPostCode) >= Long.parseLong(min)  &&  Long.parseLong(selectedPostCode) <= Long.parseLong(max))
                return true;
        }

        return isMatched;
    }

    public void RequestShippingMethods(int zone_id) {

        dialogLoader.showProgressDialog();

        int id = zone_id;

        Call<List<ShippingMethods>> call = APIClient.getInstance()
                .getShippingMethods
                        (
                                String.valueOf(zone_id)
                        );

        call.enqueue(new Callback<List<ShippingMethods>>() {
            @Override
            public void onResponse(Call<List<ShippingMethods>> call, retrofit2.Response<List<ShippingMethods>> response) {

                //String str = new Gson().toJson(response);

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {

                    if (response.body().size() > 0) {

                        addShippingMethodsToList(response.body());

                    }
                    else {
                        Toast.makeText(getContext(), "No Shipping methods", Toast.LENGTH_SHORT).show();
                    }

                }
                else {
                    Toast.makeText(getContext(), "No Shipping methods", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<ShippingMethods>> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getContext(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
                Toast.makeText(getContext(), "No Shipping methods", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void addShippingMethodsToList(List<ShippingMethods> sMethodsList) {

        List<CartDetails> cartItems = user_cart_db.getCartItems();
        double total_amount = getProductsSubTotal(cartItems);


        for (int i = 0;  i<sMethodsList.size();  i++) {
            if (sMethodsList.get(i).isEnabled())

                if(sMethodsList.get(i).getMethodId().equalsIgnoreCase("free_shipping") ) {
                    double check_min_amount = Double.parseDouble(sMethodsList.get(i).getSettings().getMin_amount().getValue());
                    if(total_amount>check_min_amount){
                        processAddedShippingMethod(sMethodsList.get(i));
                        //shippingMethodsList.add(sMethodsList.get(i));
                    }
                } else {
                    processAddedShippingMethod(sMethodsList.get(i));
                    //shippingMethodsList.add(sMethodsList.get(i));
                }
        }

        if (shippingMethodsList.size()>0){
            selectedShippingMethod = shippingMethodsList.get(0);
        }

        if (selectedShippingMethod != null) {

                    OrderDetails orderDetails = ((App) getContext().getApplicationContext()).getOrderDetails();


                    List<OrderShippingMethod> orderShippingMethodList = new ArrayList<>();
                    OrderShippingMethod orderShippingMethod = new OrderShippingMethod();

                    String str = selectedShippingMethod.getId();
                    String str2 = selectedShippingMethod.getMethodId();

                    orderShippingMethod.setShippingID(selectedShippingMethod.getId());
                    orderShippingMethod.setMethodId(selectedShippingMethod.getMethodId());
                    orderShippingMethod.setMethodTitle(selectedShippingMethod.getMethodTitle());
                    orderShippingMethod.setTotal(selectedShippingMethod.getCost());

                    orderShippingMethodList.add(orderShippingMethod);

                    orderDetails.setOrderShippingMethods(orderShippingMethodList);

                    ((App) getContext().getApplicationContext()).setOrderDetails(orderDetails);

                    ((App) getContext().getApplicationContext()).setShippingService(orderShippingMethod);

                    // Navigate to Checkout Fragment
                    Fragment fragment = new Checkout();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.main_fragment, fragment)
                            .addToBackStack(null).commit();



        }
    }

    private double getProductsSubTotal(List<CartDetails> checkoutItemsList) {

        double finalPrice = 0;

        for (int i=0;  i<checkoutItemsList.size();  i++) {
            // Add the Price of each Cart Product to finalPrice
            finalPrice += Double.parseDouble(checkoutItemsList.get(i).getCartProduct().getTotalPrice());
        }

        return finalPrice;
    }

    private void processAddedShippingMethod(ShippingMethods shippingMethod) {
        if (shippingMethod.getSettings() != null) {
            if (shippingMethod.getSettings().getCost() != null) {
                if (!TextUtils.isEmpty(shippingMethod.getSettings().getCost().getValue())) {
                    shippingMethod.setCost(shippingMethod.getSettings().getCost().getValue());
                }
                else {
                    shippingMethod.setCost("0");
                }
            }
            else if (shippingMethod.getMethodId().equalsIgnoreCase("free_shipping")) {
                if (shippingMethod.getSettings().getMin_amount() != null) {
                    if (!TextUtils.isEmpty(shippingMethod.getSettings().getMin_amount().getValue())) {
                        shippingMethod.setCost("0.00");
                    }
                }
                else {
                    shippingMethod.setCost("0");
                }
            }
            else {
                shippingMethod.setCost("0");
            }
        }
        else {
            shippingMethod.setCost("0");
        }
        shippingMethodsList.add(shippingMethod);
    }


    
}

