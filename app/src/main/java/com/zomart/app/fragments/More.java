package com.zomart.app.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zomart.app.R;
import com.zomart.app.activities.IntroScreen;
import com.zomart.app.activities.Login;
import com.zomart.app.activities.MainActivity;
import com.zomart.app.app.App;
import com.zomart.app.app.MyAppPrefsManager;
import com.zomart.app.constant.ConstantValues;
import com.zomart.app.models.device_model.AppSettingsDetails;
import com.zomart.app.utils.Utilities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import static android.content.Context.MODE_PRIVATE;

public class More extends Fragment {

    private AppSettingsDetails appSettings;

    private TextView push_notifications,
            edit_profile,
            orders,
            rewards,
            favorites,
            intro,
            add_address,
            news,
            contact_us,
            about_us,
            share_app,
            rate_app,
            download,
            settings;
    private AppCompatButton btn_login;

    private FragmentManager fragmentManager;
    private Fragment fragment;
    private SharedPreferences sharedPreferences;
    private MyAppPrefsManager myAppPrefsManager;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_more, container, false);
        init_views(rootView);
        init_events();
        init_fragment();
        return rootView;
    }

    private void init_views(View v) {
        //TextView
        push_notifications = v.findViewById(R.id.push_notifications);
        edit_profile = v.findViewById(R.id.edit_profile);
        orders = v.findViewById(R.id.orders);
        rewards = v.findViewById(R.id.rewards);
        favorites = v.findViewById(R.id.favorites);
        intro = v.findViewById(R.id.intro);
        add_address = v.findViewById(R.id.add_address);
        news = v.findViewById(R.id.news);
        contact_us = v.findViewById(R.id.contact_us);
        about_us = v.findViewById(R.id.about_us);
        share_app = v.findViewById(R.id.share_app);
        rate_app = v.findViewById(R.id.rate_app);
        download = v.findViewById(R.id.download);
        settings = v.findViewById(R.id.settings);
        //Button
        btn_login = v.findViewById(R.id.btn_login);

    }

    private void init_events() {
        fragmentManager = ((MainActivity) getActivity()).getSupportFragmentManager();
        myAppPrefsManager = new MyAppPrefsManager(getContext());
        sharedPreferences = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE);
        push_notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("isMenuItem", true);

                fragment = new NotificationFrag();
                fragment.setArguments(bundle);
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(getString(R.string.actionHome)).commit();
            }
        });
        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConstantValues.IS_USER_LOGGED_IN) {
                    // Navigate to Update_Account Fragment
                    fragment = new Update_Account();
                    fragmentManager.beginTransaction()
                            .replace(R.id.main_fragment, fragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .addToBackStack(getString(R.string.actionHome)).commit();
                }else {
                    // Navigate to Login Activity
                    startActivity(new Intent(getContext(), Login.class));
                    getActivity().finish();
                    getActivity().overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_left);
                }
            }
        });
        orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConstantValues.IS_USER_LOGGED_IN) {

                    // Navigate to My_Orders Fragment
                    fragment = new My_Orders();
                    fragmentManager.beginTransaction()
                            .replace(R.id.main_fragment, fragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .addToBackStack(getString(R.string.actionHome)).commit();

                }
                else {
                    // Navigate to Login Activity
                    startActivity(new Intent(getContext(), Login.class));
                    getActivity().finish();
                    getActivity().overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_left);
                }
            }
        });
        rewards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("isMenuItem", true);

                // Navigate to Products Fragment
                fragment = new Points_Fragment();
                fragment.setArguments(bundle);
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(getString(R.string.actionHome)).commit();
            }
        });
        favorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Navigate to WishList Fragment
                fragment = new WishList();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(getString(R.string.actionHome)).commit();
            }
        });
        intro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Navigate to IntroScreen
                startActivity(new Intent(getContext(), IntroScreen.class));
            }
        });
        add_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("shipping", "1");
                // Navigate to News Fragment
                fragment = new Shipping_Address();
                fragment.setArguments(bundle);
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(getString(R.string.actionHome)).commit();
            }
        });
        news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new News();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(getString(R.string.actionHome)).commit();
            }
        });
        contact_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Navigate to ContactUs Fragment
                fragment = new ContactUs();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(getString(R.string.actionHome)).commit();
            }
        });
        about_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Navigate to About Fragment
                fragment = new About();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(getString(R.string.actionHome)).commit();
            }
        });
        share_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Share App with the help of static method of Utilities class
                Utilities.shareMyApp(getContext());
            }
        });
        rate_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Rate App with the help of static method of Utilities class
                Utilities.rateMyApp(getContext());
            }
        });
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new Download();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(getString(R.string.actionHome)).commit();
            }
        });
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Navigate to SettingsFragment Fragment
                fragment = new SettingsFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(getString(R.string.actionHome)).commit();
            }
        });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Logout
                if (ConstantValues.IS_USER_LOGGED_IN) {
                    // Edit UserID in SharedPreferences
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("userID", "");
                    editor.putString("userCookie", "");
                    editor.putString("userPicture", "");
                    editor.putString("userName", "");
                    editor.putString("userDisplayName", "");
                    editor.apply();

                    // Set UserLoggedIn in MyAppPrefsManager
                    MyAppPrefsManager myAppPrefsManager = new MyAppPrefsManager(getContext());
                    myAppPrefsManager.setUserLoggedIn(false);

                    // Set isLogged_in of ConstantValues
                    ConstantValues.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();


                    // Navigate to Login Activity
                    startActivity(new Intent(getContext(), Login.class));
                    getActivity().finish();
                    getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);
                }
                //Login
                else {
                    startActivity(new Intent(getContext(), Login.class));
                    getActivity().finish();
                    getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);
                }
            }
        });
    }

    private void init_fragment() {
        // Set the Title of Toolbar
        try {
            //((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.actionMore));
            ((MainActivity)getActivity()).setMyTitle(getString(R.string.actionMore));
        }catch (Exception e){}
        //App Settings
        appSettings = ((App) getActivity().getApplicationContext()).getAppSettingsDetails();
        init_visible_views();
    }

    private void init_visible_views(){
        if (appSettings != null) {
            if ("1".equalsIgnoreCase(appSettings.getOne_signal_notification()))
                push_notifications.setVisibility(View.VISIBLE);
            if ("1".equalsIgnoreCase(appSettings.getEditProfilePage()))
                edit_profile.setVisibility(View.VISIBLE);
            if ("1".equalsIgnoreCase(appSettings.getMyOrdersPage()))
                orders.setVisibility(View.VISIBLE);
            if ("1".equalsIgnoreCase(appSettings.getWp_point_reward()))
                rewards.setVisibility(View.VISIBLE);
            if ("1".equalsIgnoreCase(appSettings.getWishListPage()))
                favorites.setVisibility(View.VISIBLE);
            if ("1".equalsIgnoreCase(appSettings.getIntroPage()))
                intro.setVisibility(View.VISIBLE);
            if ("1".equalsIgnoreCase(appSettings.getBill_ship_info()))
                if (ConstantValues.IS_USER_LOGGED_IN) {
                    add_address.setVisibility(View.VISIBLE);
                }
            if ("1".equalsIgnoreCase(appSettings.getNewsPage()))
                news.setVisibility(View.VISIBLE);
            if ("1".equalsIgnoreCase(appSettings.getContactUsPage()))
                contact_us.setVisibility(View.VISIBLE);
            if ("1".equalsIgnoreCase(appSettings.getAboutUsPage()))
                about_us.setVisibility(View.VISIBLE);
            if ("1".equalsIgnoreCase(appSettings.getShareApp()))
                share_app.setVisibility(View.VISIBLE);
            if ("1".equalsIgnoreCase(appSettings.getRateApp()))
                rate_app.setVisibility(View.VISIBLE);
            if ("1".equalsIgnoreCase(appSettings.getDownloads()))
                if (ConstantValues.IS_USER_LOGGED_IN) {
                    download.setVisibility(View.VISIBLE);
                }
            if ("1".equalsIgnoreCase(appSettings.getSettingPage()))
                settings.setVisibility(View.VISIBLE);

            // Add last Header Item in Drawer Header List
            if (ConstantValues.IS_USER_LOGGED_IN) {
                btn_login.setText(getResources().getString(R.string.actionLogout));
            } else {
                btn_login.setText(getResources().getString(R.string.actionLogin));
            }
        }else {
            edit_profile.setVisibility(View.VISIBLE);
            orders.setVisibility(View.VISIBLE);
            favorites.setVisibility(View.VISIBLE);
            if(ConstantValues.IS_USER_LOGGED_IN){
                rewards.setVisibility(View.VISIBLE);
            }
            intro.setVisibility(View.VISIBLE);
            if (ConstantValues.IS_USER_LOGGED_IN) {
                add_address.setVisibility(View.VISIBLE);
            }
            news.setVisibility(View.VISIBLE);
            about_us.setVisibility(View.VISIBLE);
            contact_us.setVisibility(View.VISIBLE);
            share_app.setVisibility(View.VISIBLE);
            rate_app.setVisibility(View.VISIBLE);
            if (ConstantValues.IS_USER_LOGGED_IN) {
                download.setVisibility(View.VISIBLE);
            }
            settings.setVisibility(View.VISIBLE);
            // Add last Header Item in Drawer Header List
            if (ConstantValues.IS_USER_LOGGED_IN) {
                btn_login.setText(getResources().getString(R.string.actionLogout));
            } else {
                btn_login.setText(getResources().getString(R.string.actionLogin));
            }
        }
    }
}
