package com.zomart.app.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.zomart.app.R;
import com.zomart.app.activities.MainActivity;
import com.zomart.app.app.MyAppPrefsManager;
import com.zomart.app.constant.ConstantValues;


public class HomePage_5 extends Fragment {

    private MyAppPrefsManager myAppPrefsManager;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.homepage_5, container, false);
        myAppPrefsManager = new MyAppPrefsManager(getContext());

        // Enable Drawer Indicator with static variable actionBarDrawerToggle of MainActivity
        //MainActivity.actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        try {
            //((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(ConstantValues.APP_HEADER);
            //((MainActivity)getActivity()).setMyTitle(ConstantValues.APP_HEADER);

            if (myAppPrefsManager.getSelectedStore().getStore_name()!=null){
                if (!myAppPrefsManager.getSelectedStore().getStore_name().equals("")){
                    //actionBar.setTitle(myAppPrefsManager.getSelectedStore().getStore_name());
                    ((MainActivity)getActivity()).setMyTitle(myAppPrefsManager.getSelectedStore().getStore_name());
                }else {
                    ((MainActivity)getActivity()).setMyTitle(ConstantValues.APP_HEADER);
                    //actionBar.setTitle(ConstantValues.APP_HEADER);
                }
            }else {
                ((MainActivity)getActivity()).setMyTitle(ConstantValues.APP_HEADER);
                // actionBar.setTitle(ConstantValues.APP_HEADER);
            }
        }catch (Exception e){}

        if (ConstantValues.IS_ADMOBE_ENABLED) {
            // Initialize InterstitialAd
            final InterstitialAd mInterstitialAd = new InterstitialAd(getActivity());
            mInterstitialAd.setAdUnitId(ConstantValues.AD_UNIT_ID_INTERSTITIAL);
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            mInterstitialAd.setAdListener(new AdListener(){
                @Override
                public void onAdLoaded() {
                    mInterstitialAd.show();
                }
            });
        }


        Fragment fragment = new Categories_6();

        Bundle bundle = new Bundle();
        bundle.putBoolean("isMenuItem", false);
        bundle.putBoolean("isHeaderVisible", false);
        fragment.setArguments(bundle);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.category1Frame, fragment).commit();

        return rootView;

    }

}

