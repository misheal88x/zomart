package com.zomart.app.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Transformers.BaseTransformer;
import com.google.gson.Gson;
import com.zomart.app.R;
import com.zomart.app.activities.PickLocationActivity;
import com.zomart.app.activities.SelectStoreActivity;
import com.zomart.app.adapters.StoresAdapter;
import com.zomart.app.app.App;
import com.zomart.app.app.MyAppPrefsManager;
import com.zomart.app.constant.ConstantValues;
import com.zomart.app.models.api_response_model.ErrorResponse;
import com.zomart.app.models.banner_model.BannerData;
import com.zomart.app.models.banner_model.BannerDetails;
import com.zomart.app.models.location_details_model.LocationDetailsResponse;
import com.zomart.app.models.store_model.StoreObject;
import com.zomart.app.network.APIClient;
import com.zomart.app.network.APIRequests;
import com.zomart.app.utils.Utilities;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.zomart.app.app.App.getContext;

public class SelectStoreFragment extends Fragment implements BaseSliderView.OnSliderClickListener{

    TextView empty;

    private RecyclerView stores_recycler;
    private List<StoreObject> stores_list;
    private StoresAdapter stores_adapter;
    private LinearLayoutManager stores_manager;

    private MyAppPrefsManager myAppPrefsManager;
    private ProgressBar loadingProgress;

    private Call<List<StoreObject>> networkCall;

    private String product_id = "";
    private String store_id = "";

    SliderLayout sliderLayout;
    PagerIndicator pagerIndicator;
    List<BannerDetails> bannerImages = new ArrayList<>();

    private Button change_location;
    private TextView change_location_title;
    private ProgressBar location_progress;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_select_store, container, false);
        init_views(rootView);
        init_events();
        init_activity();
        return rootView;
    }

    private void init_views(View v) {
        //RecyclerView
        stores_recycler = v.findViewById(R.id.categories_recycler);
        //TextView
        empty = v.findViewById(R.id.empty_record_text);
        change_location_title = v.findViewById(R.id.change_location_text);
        //SharedPreferences
        myAppPrefsManager = new MyAppPrefsManager(getContext());
        //ProgressBar
        loadingProgress = v.findViewById(R.id.progress);
        //Slider Layout
        sliderLayout =  v.findViewById(R.id.banner_slider);
        //Pager Indicator
        pagerIndicator =  v.findViewById(R.id.banner_slider_indicator);
        //Button
        change_location = v.findViewById(R.id.change_location);
        //ProgressBar
        location_progress = v.findViewById(R.id.get_location_details_progress);
    }

    private void init_events() {
        change_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final LocationManager manager = (LocationManager) getActivity().getSystemService( Context.LOCATION_SERVICE );
                if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                    buildAlertMessageNoGps();
                }else {
                    startActivity(new Intent(getContext(), PickLocationActivity.class));
                    getActivity().finish();
                }


            }
        });
    }

    private void getCurrentLocationDetails(){
        String lat = myAppPrefsManager.getMyLat();
        String lng = myAppPrefsManager.getMyLng();
        String add = "Your address";
        Geocoder geoCoder = new Geocoder(getContext(), Locale.getDefault());
        try {
            List<Address> addresses = geoCoder.getFromLocation(Double.valueOf(lat),Double.valueOf(lng),1);


            if (addresses.size() > 0)
            {
                for (int i=0; i<addresses.get(0).getMaxAddressLineIndex();i++)
                    add += addresses.get(0).getCountryName() + ", "+addresses.get(0).getLocality();
                change_location_title.setText(getResources().getString(R.string.change_location_text)+"\n"+add);
            }else {
                callCurrentLocationDetails();
            }


        }
        catch (IOException e1) {
            e1.printStackTrace();
            callCurrentLocationDetails();
        }

    }

    private void callCurrentLocationDetails(){
        location_progress.setVisibility(View.VISIBLE);

        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.MINUTES)
                .readTimeout(10, TimeUnit.MINUTES)
                .writeTimeout(10, TimeUnit.MINUTES).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl("https://maps.google.com/maps/api/").addConverterFactory(GsonConverterFactory.create()).build();
        APIRequests api = retrofit.create(APIRequests.class);
        Call<LocationDetailsResponse> call = api.getLocationDetails(getResources().getString(R.string.google_api_app_id),
                myAppPrefsManager.getMyLat()+","+myAppPrefsManager.getMyLng(),"true");
        call.enqueue(new Callback<LocationDetailsResponse>() {
            @Override
            public void onResponse(Call<LocationDetailsResponse> call, Response<LocationDetailsResponse> response) {
                location_progress.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body()!=null){
                        if (response.body().getResults()!=null){
                            if (response.body().getResults().size()>0){
                                change_location_title.setText(getResources().getString(R.string.change_location_text)+"\n"+
                                        response.body().getResults().get(0).getFormatted_address());
                            }else {
                                change_location_title.setText(getResources().getString(R.string.change_location_text)+"\n"+"Your address");
                            }
                        }else {
                            change_location_title.setText(getResources().getString(R.string.change_location_text)+"\n"+"Your address");
                        }
                    }else {
                        change_location_title.setText(getResources().getString(R.string.change_location_text)+"\n"+"Your address");
                    }

                }else {
                    change_location_title.setText(getResources().getString(R.string.change_location_text)+"\n"+"Your address");
                }
            }

            @Override
            public void onFailure(Call<LocationDetailsResponse> call, Throwable t) {
                Toast.makeText(getContext(), getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                location_progress.setVisibility(View.GONE);
                change_location_title.setText(getResources().getString(R.string.change_location_text)+"\n"+"Your address");
            }
        });
    }

    private void buildAlertMessageNoGps(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(getResources().getString(R.string.no_gps_text))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.no_gps_yes), new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(getResources().getString(R.string.no_gps_ignore), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void init_activity() {
        getCurrentLocationDetails();
        init_recycler();
        // Get BannersList from ApplicationContext
        try {
            BannerDetails[] b = new Gson().fromJson(getArguments().getString("banners"), BannerDetails[].class);
            if (b.length>0){
                for (BannerDetails bb : b){
                    bannerImages.add(bb);
                }
            }
        }catch (Exception e){}
        setupBannerSlider(bannerImages);
    }

    private void init_recycler(){

        if (getArguments().containsKey(ConstantValues.PRODUCT_ID)){
            if (getArguments().getString(ConstantValues.PRODUCT_ID)!=null){
                if (!getArguments().getString(ConstantValues.PRODUCT_ID).equals("")){
                    product_id = getArguments().getString(ConstantValues.PRODUCT_ID);
                }
            }
        }

        if (getArguments().containsKey(ConstantValues.STORE_ID)){
            if (getArguments().getString(ConstantValues.STORE_ID)!=null){
                if (!getArguments().getString(ConstantValues.STORE_ID).equals("")){
                    store_id = getArguments().getString(ConstantValues.STORE_ID);
                }
            }
        }
        stores_list = new ArrayList<>();
        stores_adapter = new StoresAdapter(getContext(),getActivity(),stores_list,product_id,store_id);
        stores_manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        stores_recycler.setLayoutManager(stores_manager);
        stores_recycler.setAdapter(stores_adapter);

        RequestStores();
    }

    public void RequestStores() {

        loadingProgress.setVisibility(View.VISIBLE);

        networkCall = APIClient.getInstance()
                .getAllStores();

        networkCall.enqueue(new Callback<List<StoreObject>>() {
            @Override
            public void onResponse(Call<List<StoreObject>> call, retrofit2.Response<List<StoreObject>> response) {

                loadingProgress.setVisibility(View.GONE);

                if (response.isSuccessful()) {

                    addStore(response.body());

                }
                else {
                    Converter<ResponseBody, ErrorResponse> converter = APIClient.retrofit.responseBodyConverter(ErrorResponse.class, new Annotation[0]);
                    ErrorResponse error;
                    try {
                        error = converter.convert(response.errorBody());
                    } catch (IOException e) {
                        error = new ErrorResponse();
                    }

//                    Toast.makeText(getContext(), "Error : "+error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<StoreObject>> call, Throwable t) {
                loadingProgress.setVisibility(View.GONE);
                if (!call.isCanceled())
                    Toast.makeText(getContext(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void addStore(List<StoreObject> storeList) {

        // Add Products to mostLikedProductList
        if (storeList.size() > 0) {
            for (StoreObject s : storeList){
                if (s.getLocation()!=null){
                    if (!s.getLocation().equals("")){
                        String [] coordinates = s.getLocation().split("\\s*,\\s*");
                        //Log.i("hgygh", "onMapClick: "+calculateDistance(coordinates[0],coordinates[1],getIntent().getStringExtra("lat"),getIntent().getStringExtra("lng")));
                        if (calculateDistance(coordinates[0],coordinates[1],myAppPrefsManager.getMyLat(),myAppPrefsManager.getMyLng())<=3000){
                            stores_list.add(s);
                        }

                    }
                }
            }
            //stores_list.addAll(storeList);
        }

        stores_adapter.notifyDataSetChanged();


        // Change the Visibility of emptyRecord Text based on ProductList's Size
        if (stores_adapter.getItemCount() == 0) {
            empty.setVisibility(View.VISIBLE);
        } else {
            empty.setVisibility(View.GONE);
        }

    }

    @Override
    public void onPause() {

        // Check if NetworkCall is being executed
        if (networkCall.isExecuted()){
            // Cancel the NetworkCall
            networkCall.cancel();
        }

        super.onPause();
    }

    private float calculateDistance(String lat1,String lng1,String lat2,String lng2){
        Location locationA = new Location("point A");
        locationA.setLatitude(Float.valueOf(lat1));
        locationA.setLongitude(Float.valueOf(lng1));

        Location locationB = new Location("point B");
        locationB.setLatitude(Float.valueOf(lat2));
        locationB.setLongitude(Float.valueOf(lng2));

        return locationA.distanceTo(locationB);
    }

    private void setupBannerSlider(final List<BannerDetails> bannerImages) {

        // Initialize new LinkedHashMap<ImageName, ImagePath>
        final LinkedHashMap<String, String> slider_covers = new LinkedHashMap<>();


        for (int i=0;  i< bannerImages.size();  i++) {
            // Get bannerDetails at given Position from bannerImages List
            BannerDetails bannerDetails =  bannerImages.get(i);

            // Put Image's Name and URL to the HashMap slider_covers
            slider_covers.put
                    (
                            "Image"+bannerDetails.getBannersTitle(),
                            bannerDetails.getBannersImage()
                    );
        }


        for(String name : slider_covers.keySet()) {
            // Initialize DefaultSliderView
            final DefaultSliderView defaultSliderView = new DefaultSliderView(getContext());

            // Set Attributes(Name, Image, Type etc) to DefaultSliderView
            defaultSliderView
                    .description(name)
                    .image(slider_covers.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);


            // Add DefaultSliderView to the SliderLayout
            sliderLayout.addSlider(defaultSliderView);
        }

        // Set PresetTransformer type of the SliderLayout
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);


        // Check if the size of Images in the Slider is less than 2
        if (slider_covers.size() < 2) {
            // Disable PagerTransformer
            sliderLayout.setPagerTransformer(false, new BaseTransformer() {
                @Override
                protected void onTransform(View view, float v) {
                }
            });

            // Hide Slider PagerIndicator
            sliderLayout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);

        } else {
            // Set custom PagerIndicator to the SliderLayout
            sliderLayout.setCustomIndicator(pagerIndicator);
            // Make PagerIndicator Visible
            sliderLayout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
        }

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        /*
        int position = sliderLayout.getCurrentPosition();
        String url = bannerImages.get(position).getBannersUrl();
        String type = bannerImages.get(position).getType();

        if (type.equalsIgnoreCase("product")) {
            if (!url.isEmpty()) {
                // Get Product Info
                Bundle bundle = new Bundle();
                bundle.putInt("itemID", Integer.parseInt(url));

                // Navigate to Product_Description of selected Product
                Fragment fragment = new Product_Description();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(getString(R.string.actionHome)).commit();
            }

        }
        else if (type.equalsIgnoreCase("category")) {
            if (!url.isEmpty()) {
                int categoryID = 0;
                String categoryName = "";

                for (int i=0;  i<allCategoriesList.size();  i++) {
                    if (url.equalsIgnoreCase(String.valueOf(allCategoriesList.get(i).getId()))) {
                        categoryName = allCategoriesList.get(i).getName();
                        categoryID = allCategoriesList.get(i).getId();
                    }
                }

                // Get Category Info
                Bundle bundle = new Bundle();
                bundle.putInt("CategoryID", categoryID);
                bundle.putString("CategoryName", categoryName);

                // Navigate to Products Fragment
                Fragment fragment = new Products();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(getString(R.string.actionHome)).commit();
            }

        }
        else if (type.equalsIgnoreCase("on_sale")) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("on_sale", true);
            bundle.putBoolean("isMenuItem", true);

            // Navigate to Products Fragment
            Fragment fragment = new Products();
            fragment.setArguments(bundle);
            getFragmentManager().beginTransaction()
                    .replace(R.id.main_fragment, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .addToBackStack(getString(R.string.actionHome)).commit();

        }
        else if (type.equalsIgnoreCase("featured")) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("featured", true);
            bundle.putBoolean("isMenuItem", true);

            // Navigate to Products Fragment
            Fragment fragment = new Products();
            fragment.setArguments(bundle);
            getFragmentManager().beginTransaction()
                    .replace(R.id.main_fragment, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .addToBackStack(getString(R.string.actionHome)).commit();

        }
        else if (type.equalsIgnoreCase("latest")) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("isMenuItem", true);

            // Navigate to Products Fragment
            Fragment fragment = new Products();
            fragment.setArguments(bundle);
            getFragmentManager().beginTransaction()
                    .replace(R.id.main_fragment, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .addToBackStack(getString(R.string.actionHome)).commit();

        }
         */
    }
}
