package com.zomart.app.models.store_model;

import com.google.gson.annotations.SerializedName;

public class StoreRateObject {
    @SerializedName("count") private float count = 0;

    public float getCount() {
        return count;
    }

    public void setCount(float count) {
        this.count = count;
    }
}
