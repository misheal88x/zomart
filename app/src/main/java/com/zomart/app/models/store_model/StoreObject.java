package com.zomart.app.models.store_model;

import com.google.gson.annotations.SerializedName;

public class StoreObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("store_name") private String store_name = "";
    @SerializedName("gravatar") private String gravatar ="";
    @SerializedName("banner") private String banner = "";
    @SerializedName("featured") private boolean featured = false;
    @SerializedName("location") private String location = "";
    @SerializedName("rating") private StoreRateObject rating = new StoreRateObject();
    @SerializedName("address") private Object address = new Object();

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public String getGravatar() {
        return gravatar;
    }

    public void setGravatar(String gravatar) {
        this.gravatar = gravatar;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public StoreRateObject getRating() {
        return rating;
    }

    public void setRating(StoreRateObject rating) {
        this.rating = rating;
    }

    public Object getAddress() {
        return address;
    }

    public void setAddress(Object address) {
        this.address = address;
    }
}
