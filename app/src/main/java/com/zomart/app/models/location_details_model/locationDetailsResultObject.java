package com.zomart.app.models.location_details_model;

import com.google.gson.annotations.SerializedName;

public class locationDetailsResultObject {
    @SerializedName("formatted_address") private String formatted_address = "";

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }
}
