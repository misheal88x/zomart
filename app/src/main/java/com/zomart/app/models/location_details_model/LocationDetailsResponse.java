package com.zomart.app.models.location_details_model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class LocationDetailsResponse {
    @SerializedName("results") private List<locationDetailsResultObject> results = new ArrayList<>();

    public List<locationDetailsResultObject> getResults() {
        return results;
    }

    public void setResults(List<locationDetailsResultObject> results) {
        this.results = results;
    }
}
