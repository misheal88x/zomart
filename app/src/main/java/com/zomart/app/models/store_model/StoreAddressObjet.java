package com.zomart.app.models.store_model;

import com.google.gson.annotations.SerializedName;

public class StoreAddressObjet {
    @SerializedName("city") private String city = "";

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
