package com.zomart.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.stripe.android.net.RequestOptions;
import com.zomart.app.R;
import com.zomart.app.activities.MainActivity;
import com.zomart.app.app.MyAppPrefsManager;
import com.zomart.app.constant.ConstantValues;
import com.zomart.app.customs.CircularImageView;
import com.zomart.app.models.store_model.StoreAddressObjet;
import com.zomart.app.models.store_model.StoreObject;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class StoresAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<StoreObject> list;
    String product_id,store_id;
    Activity activity;

    public StoresAdapter(Context context, Activity activity, List<StoreObject> list, String product_id, String store_id) {
        this.context = context;
        this.list = list;
        this.product_id = product_id;
        this.store_id = store_id;
        this.activity = activity;
    }



    //********** Called to Inflate a Layout from XML and then return the Holder *********//

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        // Inflate the custom layout
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_store, parent, false);
        return new MyViewHolder(itemView);
    }

    //********** Called by RecyclerView to display the Data at the specified Position *********//

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final MyViewHolder holder0 = (MyViewHolder)holder;
        StoreObject store = list.get(position);

        if (store.getStore_name()!=null&&!store.getStore_name().equals("")){
            holder0.name.setText(store.getStore_name());
        }else {
            holder0.name.setText(context.getResources().getString(R.string.no_name));
        }

        try {
            /*
            Glide.with(context)
                    .load(store.getGravatar())
                    .error(R.drawable.placeholder)
                    .into(holder0.icon);
                    **/

            Glide.with(context)
                    .load(store.getGravatar())
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            holder0.icon.setVisibility(View.GONE);
                            holder0.name.setVisibility(View.VISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            holder0.icon.setVisibility(View.VISIBLE);
                            holder0.name.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .error(R.drawable.placeholder)
                    .into(holder0.icon);
        }catch (Exception e){}

        try {
            /*
            Glide.with(context)
                    .load(store.getBanner())
                    .error(R.drawable.placeholder)
                    .into(holder0.cover);

             */
            Glide.with(context)
                    .load(store.getBanner())
                    .error(R.drawable.placeholder)
                    .into(holder0.cover);
        }catch (Exception e){}

        try{
            holder0.rate.setRating(store.getRating().getCount());
        }catch (Exception e){}

        try {
            String j= new Gson().toJson(store.getAddress());
            StoreAddressObjet o = new Gson().fromJson(j,StoreAddressObjet.class);
            holder0.region.setText(o.getCity());
        }catch (Exception e){}

        holder0.progressBar.setProgressMax(100);
        holder0.progressBar.setProgress(45);
    }



    //********** Returns the total number of items in the data set *********//

    @Override
    public int getItemCount() {
        return list.size();
    }



    /********** Custom ViewHolder provides a direct reference to each of the Views within a Data_Item *********/

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView icon;
        LinearLayout layout;
        ImageView cover;
        TextView name,region;
        CircularProgressBar progressBar;
        SimpleRatingBar rate;


        public MyViewHolder(final View itemView) {
            super(itemView);
            layout =  itemView.findViewById(R.id.layout);
            name = (TextView) itemView.findViewById(R.id.name);
            cover = itemView.findViewById(R.id.cover);
            icon =  itemView.findViewById(R.id.icon);
            region = itemView.findViewById(R.id.region);
            progressBar = itemView.findViewById(R.id.progress);
            rate = itemView.findViewById(R.id.stars);
            layout.setOnClickListener(this);
        }

        // Handle Click Listener on Category item
        @Override
        public void onClick(View view) {

            MyAppPrefsManager myAppPrefsManager = new MyAppPrefsManager(context);
            myAppPrefsManager.setSelectedStore(list.get(getAdapterPosition()));
            Intent intent = new Intent(context, MainActivity.class);
            if (!product_id.equals("")){
                intent.putExtra(ConstantValues.PRODUCT_ID,product_id);
            }
            if (!store_id.equals("")){
                intent.putExtra(ConstantValues.STORE_ID,store_id);
            }
            context.startActivity(intent);
            activity.finish();
        }
    }
}
